// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Electrometer Support Library
//
// = FILENAME
//    TangoEthernetLink.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _TANGO_ETHERNET_LINK_H_
#define _TANGO_ETHERNET_LINK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include "CommunicationLink.h"

/**
 *  \addtogroup Communication Management
 *  @{
 */

/**
 *  \brief This class manage the ETHERNET communication bus
 *
 *  \author
 *  \date
 */

class TangoEthernetLink : public CommunicationLink
{

public :
 	/**
	* \brief Initialization.
	*/
	TangoEthernetLink (std::string& serial_device_name);

	/**
	* \brief Release resources.
	*/
	~TangoEthernetLink ();

	/**
	*  \brief Send command (data) as string to hardware.
	*
	*  \throws Tango::DevFailed
	*/
	void write(std::string cmd)	 throw (Tango::DevFailed);

	std::string read(void) throw (Tango::DevFailed);

	/**
	*  \brief Performs a write read operation as string. (read nb char)
	*
	*  \throws Tango::DevFailed
	*/
	std::string write_read(std::string cmd, size_t nbChar)  throw (Tango::DevFailed);

private :

  /**
	* Creates a proxy on the specified Serial Device.
	*/
	void check_proxy(void) throw (Tango::DevFailed);

	Tango::DeviceProxy* _ethernet_proxy;

	bool _is_ethernet_proxy_ok;

};

/** @} */	//- end addtogroup

#endif // _TANGO_ETHERNET_LINK_H_
