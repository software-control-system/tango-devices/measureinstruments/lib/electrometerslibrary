// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Electrometer Support Library
//
// = FILENAME
//    CommunicationLink.h
//
// = AUTHOR
//    X. Elattaoui
//
//
// $Author: xavela $
//
// $Revision: 1.10 $
//
// $Log: not supported by cvs2svn $
// Revision 1.9  2012/07/11 08:21:54  xavela
// for test purpose only !
//
// Revision 1.8  2012/07/09 13:15:59  xavela
// Novelec part : bug at init fixed.
//
// Revision 1.7  2012/07/03 06:15:53  xavela
// Novelec part : bug at init fixed.
//
// Revision 1.6  2010/06/10 14:36:15  xavela
// cmd switch_off called before any parameters modifications and then locked with switch_on.
//
// Revision 1.5  2008/04/15 12:51:59  xavela
// xavier :
// - SRQ management changed :
// * ReadStatusByteRegister method added
// * IsSrqLineUP added
//
// Revision 1.4  2008/02/15 10:17:55  xavela
// xavier :
// - command abort added for SCPI Keithleys
//
// Revision 1.3  2008/02/13 15:51:44  xavela
// xavier :
// - Integration Mode available for DDC/SCPI devices
// - tests done with a K617 (DDC) and a K6485 (SCPI)
//
// Revision 1.2  2008/02/11 16:55:04  xavela
// xavier : for DDC part
// - integration mode OK (tests done with K_486)
//
// - TODO: DDC/SCPI
// integration time for ScanServer compatibility.
// report config for all DDC
// add configuration in SCPI start command
//
// Revision 1.1  2007/07/09 13:20:35  stephle
// initial import
//
//
// ============================================================================

#ifndef _COMMUNICATION_LINK_H_
#define _COMMUNICATION_LINK_H_

#include <string>

// ============================================================================
// DEPENDENCIES
// ============================================================================


/**
 *  \addtogroup Communication Management
 *  @{
 */

/**
 *  \brief Abstract class to manage a specific communication bus
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class CommunicationLink
{
public :
	
	/**
	* Initialization. 
	*/
	CommunicationLink (std::string& communication_link_name);
	
	/**
	* Release resources.
	*/
	virtual ~CommunicationLink (void);

	/**
	*  \brief Send command (data) as string to hardware.
	*
	*  \throws Tango::DevFailed
	*/
	virtual void write(std::string)	= 0; 
	
	/**
	*  \brief Gets hardware response as string.
	*
	*  \throws Tango::DevFailed
	*/
	virtual std::string read(void)	= 0;

	/**
	*  \brief Performs a write read operation as string.
	*
	*  \throws Tango::DevFailed
	*/
	virtual std::string write_read(std::string cmd_to_send, size_t readLength=0) = 0;

	/**
	*  \brief Performs a write read operation as string. (for Serialline )
	*
	*  \throws Tango::DevFailed
	*/
	//virtual std::string write_read (std::string , long ) {return "";}


	/**
	*  \brief Returns the Gpib SRQ line state : used here to know if Keithley 
  *           device is ready after an integration cycle.
	*
	*  \throws Tango::DevFailed
	*/
	virtual bool SRQLineState             (void); //- used to know if the integration cycle is done! 
	virtual short readStatusByteRegister  (void); //- device status byte register value on SRQ! 

	/**
	*  \brief Clear a specific device (same as reset *RST).
	*
	*  \throws Tango::DevFailed
	*/
	virtual void clear(void);

  /**
	*  \brief Trigg device on the gpib bus.
	*
	*  \throws Tango::DevFailed
	*/
	virtual void trigger(void);


protected :
	std::string _communication_Device_name;

};

/** @} */	//- end addtogroup

#endif // _COMMUNICATION_LINK_H_
