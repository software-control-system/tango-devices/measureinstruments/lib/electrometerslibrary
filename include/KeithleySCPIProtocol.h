// ============================================================================
//
// = CONTEXT
//    TANGO Project - SCPI Keithley Electrometer Support Library
//
// = FILENAME
//    KeithleySCPIProtocol.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _KEITHLEY_SCPI_PROTOCOL_H_
#define _KEITHLEY_SCPI_PROTOCOL_H_

#include <vector>
#include "ElectrometerProtocol.h"
#include "TangoGpibLink.h"
#include "ElectrometerException.h"

/**
 *  \addtogroup Standard Commands Protocol 
 *  @{
 */

/**
 *  \brief This class manage the Keithley SCPI Commands Protocol
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class KeithleySCPIProtocol : public ElectrometerProtocol
{
public:

	/**
	*  \brief Initialization. 
	*/
	KeithleySCPIProtocol (std::string& gpib_device_name);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~KeithleySCPIProtocol (void);

	/**
	*  \brief create the communication device proxy.
	*/
  bool build_communicationLink();

	/**
	*  \brief Functions to save/restore specifics configuration.
	*		NOTE : only available fro SCPI devices
	*/
	void save_configuration(unsigned short memoryIdx);
	void restore_configuration(unsigned short memoryIdx);

	/**
	*  \brief Electrometer Functions.
	*/
	void set_range			  (std::string value) ;
	void autoRange_on		  (void);
	void autoRange_off		(void);
	std::string is_autoRange_on(void);

	void zero_check_on		(void);
	void zero_check_off		(void);
	void zero_correct_on	(void);
	void zero_correct_off	(void);
	void zero_correct_state_on (void);
	void zero_correct_state_off(void);
	void auto_zero_on		  (void);
	void auto_zero_off		(void);
	void reset				    (void);
  void clear_registers	(void);

	/**
	*  \brief Electrometer Mode.
	*/
	void setAmperMeterMode	(void);
	void setVoltMeterMode	  (void);
	void setOhmMeterMode	  (void);
	void setCoulombMeterMode(void);
	/**
	*  \brief VSource operations.
	*/
  void setVSourceOutputON (void);
  void setVSourceOutputOFF(void);
  void setVSourceValue     (double volts);
  std::string getVSourceValue   (void);

	/**
	*  \brief Electrometer : cmd to get electrometer data.
	*/
	std::string get_value                   (void);
	std::vector<double> get_integratedValue (void);
	std::vector<double> get_fetchValue      (void);

 	/**
	*  \brief Electrometer : cmd used to know if the integration cycle is done!
	*/
	bool SRQLineState             (void); //- used to know if the integration cycle is done! 
	short readStatusByteRegister  (void); //- device status byte register value on SRQ! 
	
  void init_keithley	(void); //- start operation(s)
	void abort        	(void); //- cancel only all operation(s) started by init_keithley()
	/**
	*  \brief Electrometer : Acquisition config
	*/
  void set_knplc            (std::string nPLC);
	void set_triggercount		  (std::string trigcounts);
	void set_triggerdelay		  (std::string trigdelay);
	void set_triggerdelayAuto	(std::string trigdelAuto);
	void set_averagecount		  (std::string avercounts);
	void set_averagecontrol		(std::string averctrl);
	void averageStateON			  (void);
	void averageStateOFF		  (void);
	std::string get_knplc			(void);
	std::string get_triggercount	  (void);
	std::string get_triggerdelay	  (void);
	std::string get_triggerdelayAuto(void);
	std::string get_averagecount	  (void);
	std::string get_averagecontrol	(void);
  void read_data_with_no_timestamp(void);
	/**
	*  \brief K_6517 specifics commands
	*/
  std::string get_averagecount_K6517  (void);
  void set_averagecount_K6517         (std::string averageCountStr);
  std::string get_averagecontrol_K6517(void);
  void set_averagecontrol_K6517       (std::string nbAverTCtrl);
	void averageStateON_K6517			      (void);
	void averageStateOFF_K6517		      (void);
	/**
	*  \brief Electrometer : Buffer config
	*/
  void set_buffer_size		    (std::string size);
  void clear_buffer		        (void);
  void store_raw_input		    (void);
  void start_storing		      (void);
  void enable_SRQBufferFull		(void);
  void disable_SRQBufferFull	(void);

  std::string get_buffer_size	(void);

	/**
	*  \brief Get Raw Electrometer Status.
	*/
	std::string get_raw_status (void) ;

	/**
	*  \brief Electrometer : getter and setter.
	*/
	std::string get_mode	(void);
	std::string get_range	(void);

	/**
	*
	*/
	void set_isDiffSuportedMode	(bool supportDiffMode) 
		{ isDiffSuportedMode = supportDiffMode; };

protected :
  std::vector<double> buildDataList (std::string listToParse);

private :
	CommunicationLink* _communication_link;
	bool isDiffSuportedMode;
  std::string trigCountStr;


};

/** @} */	//- end addtogroup

#endif // _KEITHLEY_SCPI_PROTOCOL_H_
