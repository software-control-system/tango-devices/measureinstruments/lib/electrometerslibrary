// ============================================================================
//
// = CONTEXT
//    TANGO Project - DDC Keithley Electrometer Support Library
//		( DDC for Device Dependent Command )
// = FILENAME
//    KeithleyDDCProtocol.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _KEITHLEY_DDC_PROTOCOL_H_
#define _KEITHLEY_DDC_PROTOCOL_H_

#include "ElectrometerProtocol.h"
#include "ElectrometerException.h"
#include "TangoGpibLink.h"

/**
 *  \addtogroup Standard Commands Protocol 
 *  @{
 */

/**
 *  \brief This class manage the Keithley DDC Commands Protocol
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class KeithleyDDCProtocol : public ElectrometerProtocol
{
public:

	/**
	*  \brief Initialization. 
	*/
	KeithleyDDCProtocol (std::string& gpib_device_name);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~KeithleyDDCProtocol (void);

	/**
	*  \brief create the communication device proxy.
	*/
  bool build_communicationLink();

  /**
	*  \brief Electrometer Functions.
	*/
	void set_range     (std::string value);
	void autoRange_on               (void);
	void autoRange_off		          (void);
	void autoRange_OFF_forK486_487  (void);
	void autoRange_OFF_forK617_6512 (void);

	void zero_check_on		(void);
	void zero_check_off		(void);
	void zero_correct_on	(void);
	void zero_correct_off	(void);
	void auto_zero_on		  (void);
	void auto_zero_off		(void);

	void reset				    (void);
//	void local				(void); // To be implemented in future library versions
//	void remote				(void);
	
 	/**
	*  \brief Electrometer : cmd used to know if the integration cycle is done!
	*/
	virtual bool SRQLineState             (void); //- used to know if the integration cycle is done! 
	virtual short readStatusByteRegister  (void); //- device status byte register value on SRQ! 
	
  /**
	*  \brief Electrometer : cmd to get electrometer data.
	*/
	std::string get_value(void);
	std::vector<double> get_integratedValue (void);
	std::vector<double> get_fetchValue      (void);

	/**
	*  \brief Electrometer Mode.
	*/
	void setAmperMeterMode	        (void);
	void setAmperMeterMode_forK487  (void);
	void setVoltMeterMode	          (void);
	void setOhmMeterMode	          (void);
	void setCoulombMeterMode        (void);
	void clear_registers	          (void);

	/**
	*  \brief Get Raw Electrometer Status.
	*/
	std::string get_raw_status (void) ;

	/**
	*  \brief Returns the DDC Keithley configuration
	*			asking its "Machine Status" word (= Using "U0X" cmd) .
	*/
	std::string get_DDC_configuration	(void);

	/**
	*  \brief Electrometer Mode : getters and setters.
	*/
	std::string get_mode        (void)	{ return _mode; }
	std::string get_range       (void);
  bool get_overloadRangeState (void) { return _is_overloaded; } //- used to know if the device range is overloaded! 

	/**
	*  \brief Methods to configure the Integration mode
	*/
  void set_buffer_size                    (std::string);
  void set_triggerMode                    (std::string);
  void enable_SRQBufferFull               (void);
  void disable_SRQBufferFull              (void);
  void set_conversionRate                 (void);
  void enable_readingWithPrefix           (void);
  void disable_readingWithPrefix          (void);
  void enable_ReadingsFromElectrometer    (void);
  void enable_readingsFromBuffer_K617_6512(void);
  void enable_readingsFromBuffer_K486_487 (void);
  //- only for K_486 and K_487 devices
  void enable_integrationPeriod           (void);

	
  
  
  
  
  /*	TODO : for next PicoLib evolution
	//- CMD B
	virtual void reading_source (void);
	//- CMD Q
	virtual void data_store (void);
	//- CMD G
	virtual void data_format (void);
	//- = REL
	virtual void baseline_suppression_ON(void);
	virtual void baseline_suppression_OFF(void);
	*/
protected :
  std::vector<double> buildDataList (std::string listToParse);

private :
	CommunicationLink* _communication_link;

  bool _is_overloaded;
};

/** @} */	//- end addtogroup

#endif // _KEITHLEY_DDC_PROTOCOL_H_
