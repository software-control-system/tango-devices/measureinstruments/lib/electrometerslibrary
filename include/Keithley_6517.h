// ============================================================================
//
// = CONTEXT
//    TANGO Project - SCPI Keithley Electrometer Support Library
//
// = FILENAME
//    Keithley_6517.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _Keithley_6517_H_
#define _Keithley_6517_H_

#include "AbstractElectrometerClass.h"

/**
 *  \addtogroup SCPI Keithley
 *  @{
 */

/**
 *  \brief This class manage 6517 Keithley type
 *
 *  \author Xavier Elattaoui
 *  \date 09-2007
 */

class Keithley_6517 : public AbstractElectrometerClass
{
public:

	/**
	*  \brief Initialization. 
	*/
	Keithley_6517 (std::string& comLink_device_name);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~Keithley_6517 (void);

	/**
	*  \brief protocole initailisation.
	*/
    bool init_protocol (void);

	/**
	*  \brief Functions to save/restore specifics configuration.
	*/
	void save_configuration(unsigned short memoryIdx);
	void restore_configuration(unsigned short memoryIdx);

	/**
	*  \brief Device dependent commands.
	*/
	void range_up   (void);
	void range_down (void);
    std::string get_ElectroMeterRange (void);
    std::vector<std::string> ranges_list(void);
    void set_ElectroMeterRange (std::size_t rangeIdx);
	
	std::vector<double> get_integratedValue (void);
	std::vector<double> get_fetchValue      (void);
	/**
	*  \brief Electrometer Mode.
	*/
	void setAmperMeterMode  (void);
	void setVoltMeterMode   (void);
	void setOhmMeterMode    (void);
	void setCoulombMeterMode(void);

	/**
	*  \brief Specifics commands.
	*/
  std::string get_averagecount  (void);
  void set_averagecount         (std::string nbAvCntStr);
  std::string get_averagecontrol(void);
  void set_averagecontrol       (std::string nbAverTCtrl);
  void averageStateON           (void);
  void averageStateOFF          (void);
	/**
	*  \brief Methods to configure the integration mode.
	*/
  void set_knplc        (double);
  void set_triggercount (short);
  void set_buffer_size  (short);
	void init_keithley		(void); 
	/**
	*  \brief The integration time (sec).
	*/
	void set_integrationTime			(double);

	/**
	*  \brief Enable/Disable the VSource Output.
	*/
  void enable_VSourceOutput (void);
  void disable_VSourceOutput(void);
  void set_VSourceValue     (double volts);
  double get_VSourceValue   (void);

	//-	TODO :
	//	SCPI_Filters*	_ddcFilters;
	//	SCPI_Triggers*	_ddcTriggers;

	/**
	*  \brief Not supported commands.
	*/
	void auto_zero_on	(void);
	void auto_zero_off	(void);

private:
	//- method to obtain the range index
	void update_range (void);
	std::string _kmode;
  double  _numPLC;
  int  _trigCounts;
	
};

/** @} */	//- end addtogroup

#endif // _Keithley_6517_H_
