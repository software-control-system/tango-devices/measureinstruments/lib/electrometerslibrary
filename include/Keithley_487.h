// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Electrometer Support Library
//
// = FILENAME
//    Keithley_487.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _KEITHLEY_487_H_
#define _KEITHLEY_487_H_

#include "AbstractElectrometerClass.h"

/**
 *  \addtogroup DDC_Keithley
 *  @{
 */

/**
 *  \brief This class manage 487 Keithley type
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class Keithley_487 : public AbstractElectrometerClass
{
public:

	/**
	*  \brief Initialization. 
	*/
	Keithley_487 (std::string& comLink_device_name);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~Keithley_487 (void);

	/**
	*  \brief protocole initailisation.
	*/
  bool init_protocol (void);

	/**
	*  \brief Electrometer methods.
	*/
	void range_up       (void);
	void range_down     (void);
	void autoRange_off  (void);
	void set_buffer_size(short);
	void set_triggerMode(short);
	void init_keithley	(void); 
	short get_buffer_size (void) { return _size; };

  /**
	*  \brief Electrometer Function(s).
	*/
	void setAmperMeterMode (void);

	/**
	*  \brief getter(s) & setter(s)	
	*/
	std::string get_ElectroMeterMode(void);
	std::string get_ElectroMeterRange(void);
    std::vector<std::string> ranges_list(void);
    void set_ElectroMeterRange (std::size_t rangeIdx);
	
	/**
	*  \brief The integration time (sec).
	*/
	void set_integrationTime			(double);

	/**
	*  \brief Electrometer status.
	*/
	std::string electrometer_status (void);

	/**
	*  \brief Returns the DDC Keithley configuration
	*			asking its "Machine Status" word (= Using "U0X" cmd) .
	*/
	std::string get_configuration	(void);
	
	//-	TODO :
	//	DDC_Filters*	_ddcFilters;
	//	DDC_Triggers*	_ddcTriggers;
	
};

/** @} */	//- end addtogroup

#endif // _KEITHLEY_487_H_
