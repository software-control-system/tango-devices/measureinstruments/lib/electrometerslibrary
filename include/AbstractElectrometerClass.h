//+=============================================================================
//
// = CONTEXT
//    TANGO Project - Abstract Electrometer Class Support Library
//
// = FILENAME
//    AbstractElectrometerClass.h
//
//
// $Author: xavela $
//
// $Revision: 1.22 $
//
// $Log: not supported by cvs2svn $
// Revision 1.21  2011/03/14 14:42:22  xavela
// SaveRestoreConfiguration command added. (Available only for SCPI Keithley devices)
//
// Revision 1.20  2010/06/10 14:36:15  xavela
// cmd switch_off called before any parameters modifications and then locked with switch_on.
//
// Revision 1.19  2010/03/17 11:57:04  xavela
// VSource control added !
//
// Revision 1.18  2010/03/17 10:00:15  xavela
// VSource output added for K_6517
//
// Revision 1.17  2009/10/14 15:26:17  xavela
// KeithleyMemory leak fixed.
// Novelec part updated
//
// Revision 1.16  2009/03/12 09:29:00  xavela
// xavier:
// Keithley part : support for 6517.
//
// Revision 1.15  2009/03/10 10:29:14  xavela
// xavier:
// range values ordered
// set_range added
//
// Revision 1.14  2009/01/19 12:00:25  xavela
// xavier :
// - MCCE2 (Novelec part) : methods to change the active channel to communicate with.
//
// Revision 1.13  2008/06/20 14:36:24  xavela
// xavier :
// - DDC status show only errors if any
// - added command to get DDC model configuration
//
// Revision 1.12  2008/05/14 09:42:37  xavela
// xavier :
// - attributes management :
// -> integrationTime, buffersize and triggerMode now well initialised
//
// TODO : after a Start command
// -> declare all attributes INVALID !?
//
// Revision 1.11  2008/04/30 15:57:29  xavela
// xavier :
// 6517 model management added and tested :
// -> problem to check : no SRQ !
//
// Revision 1.10  2008/04/15 12:51:59  xavela
// xavier :
// - SRQ management changed :
// * ReadStatusByteRegister method added
// * IsSrqLineUP added
//
// Revision 1.9  2008/02/15 10:17:55  xavela
// xavier :
// - command abort added for SCPI Keithleys
//
// Revision 1.8  2008/02/15 10:15:16  xavela
// xavier :
// - command abort added for SCPI Keithleys
//
// Revision 1.7  2008/02/15 08:49:46  xavela
// xavier :
// - minor change for integration time
//
// Revision 1.6  2008/02/13 15:51:44  xavela
// xavier :
// - Integration Mode available for DDC/SCPI devices
// - tests done with a K617 (DDC) and a K6485 (SCPI)
//
// Revision 1.5  2008/02/11 16:55:04  xavela
// xavier : for DDC part
// - integration mode OK (tests done with K_486)
//
// - TODO: DDC/SCPI
// integration time for ScanServer compatibility.
// report config for all DDC
// add configuration in SCPI start command
//
// Revision 1.4  2008/02/08 17:24:32  xavela
// xavier : for DDC part
// - trigger mode and buffer size management added.
// - TODO: DDC/SCPI
// integration time for ScanServer compatibility.
// SRQ management for Keithley Integration mode
//
// Revision 1.3  2007/11/28 10:37:30  xavela
// compile en MODE debug ->
// modifier le(s) MakeFile !
//
// Revision 1.2  2007/11/27 10:17:46  xavela
// xavier :
// - modif done on Novelec part :
// init_MCCE2() added
//
// Revision 1.1  2007/07/09 13:20:35  stephle
// initial import
//
//

// ============================================================================

#ifndef _ABSTRACT_ELECTROMETER_CLASS_H_
#define _ABSTRACT_ELECTROMETER_CLASS_H_

#include <tango.h>  //- Tango exceptions
#include "ElectrometerProtocol.h"

/**
 *  \brief Visible class from the DServer
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */
class AbstractElectrometerClass
{
public:

	/**
	*  \brief Initialization. 
	*/
	AbstractElectrometerClass (std::string comLink_device_name);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~AbstractElectrometerClass (void);

	AbstractElectrometerClass& operator= (const AbstractElectrometerClass& src);

	/**
	*  \brief Electrometer common functions.
	*/
  virtual bool init_protocol (void) = 0;
  virtual void range_up	     (void) = 0;
  virtual void range_down    (void) = 0;
  virtual void reset		(void); 
  virtual void local		(void);
  virtual void remote		(void);

	/**
	*  \brief Functions to save/restore specifics configuration.
	*		NOTE : only available fro SCPI devices
	*/
	virtual void save_configuration(unsigned short memoryIdx);
	virtual void restore_configuration(unsigned short memoryIdx);

	/**
	*  \brief Electrometer : cmd to get electrometer data.
	*/
	std::string get_value	(void);
	virtual std::vector<double> get_integratedValue	(void);
	virtual std::vector<double> get_fetchValue      (void);

	/**
	*  \brief Keithley Electrometer methods
	*/
	void autoRange_on		  (void);
	std::string is_autoRange_on(void);
	
	void zero_check_on		(void);
	void zero_check_off		(void);
	void zero_correct_on	(void);
	void zero_correct_off	(void);
	virtual void autoRange_off		(void);
	virtual void auto_zero_on		  (void);
	virtual void auto_zero_off		(void);
	virtual void setAmperMeterMode(void);
	virtual void setVoltMeterMode	(void);
	virtual void setOhmMeterMode	(void);
	virtual void setCoulombMeterMode(void);

	/**
	*  \brief  Following functions are only supported for SCPI protocol
	*
	*/
	void abort (void); 
	virtual void init_keithley		(void); 
	void clear_registers			    (void); 
	virtual void averageStateON		(void); 
	virtual void averageStateOFF	(void); 
	virtual void set_knplc			  (float nPLC);
	virtual void set_triggercount	(short trigcounts);
	void set_triggerdelay			    (std::string trigdelay);
	void set_triggerdelayAuto		  (std::string trigdelAuto);
	virtual void set_averagecount	(std::string avercounts);
	virtual void set_averagecontrol	(std::string averctrl);
	std::string get_knplc			    (void);
	std::string get_triggercount	(void);
	std::string get_triggerdelay	(void);
	std::string get_triggerdelayAuto(void);
	virtual std::string get_averagecount	(void);
	virtual std::string get_averagecontrol(void);
	virtual void clear_buffer				(void);
	virtual void store_raw_input		(void);
	virtual void start_storing			(void);
	void enable_SRQBufferFull				(void);
	void disable_SRQBufferFull			(void);
	virtual bool SRQLineState				(void); //- used to know if the integration cycle is done! 
	virtual short readStatusByteRegister    (void); //- device status byte register value on SRQ! 
	bool get_overloadRangeState				(void);		//- used to know if the device range is overloaded (DDC Keithley ONLY)! 
	virtual void set_buffer_size			(short);
	virtual void set_triggerMode			(short);
	virtual void set_integrationTime	(double);
	virtual short get_buffer_size			(void);
	virtual short get_triggerMode			(void) { return _trigMod;}
// 	virtual void set_range(std::size_t idx) = 0;

	/**
	*  \brief  VSource operations (only for K_6517)
	*
	*/
  virtual void enable_VSourceOutput (void);
  virtual void disable_VSourceOutput(void);
  virtual void set_VSourceValue     (double volts);
  virtual double get_VSourceValue   (void);

	/**
	*  \brief Novelec Electrometer methods
	*
	* Following functions are only supported for Novelec Protocol
	*/
	virtual void set_Zero_VonF_function			(void);
	virtual void set_Offset_ZeroV1_function	(void);
	virtual void set_Leakage_ZeroV2_function(void);
	virtual void set_Test_function			    (void);
	virtual void set_Measure_function		    (void);
	virtual void mcce_on    (void);
	virtual void mcce_off   (void);
	virtual void mcce_init  (void);
	/**
	*  \brief Novelec getters and setters
	*/
	virtual std::string get_ElectroMeterPolarity  (void);
	virtual std::string get_ElectroMeterFrequency (void);
	virtual std::string get_ElectroMeterGain      (void);
	virtual unsigned short get_ElectroChannel	    (void);
    virtual void set_ElectroMeterRange          (std::string rangeStr);
    virtual void set_ElectroMeterRange          (std::size_t rangeIdx);
	virtual void set_ElectroMeterPolarity		(std::string pola);
	virtual void set_ElectroMeterFrequency	(std::string freq);
	virtual void set_ElectroMeterGain			  (std::string gain);
	virtual void set_ElectroChannel				  (unsigned short address);

	/**
	*  \brief Electrometer Status and State.
	*/
	virtual std::string electrometer_status	(void);

	/**
	*  \brief Returns the DDC Keithley configuration
	*			asking its "Machine Status" word (= Using "U0X" cmd) .
	*/
	virtual std::string get_configuration	(void);

	/**
	*  \brief Common getters and setters
	*/
	virtual std::string get_ElectroMeterMode	(void);
	virtual std::string get_ElectroMeterRange	(void);
    virtual std::vector<std::string> ranges_list(void)=0;


protected :

	/**
	*  \brief Electrometer State
	*/
	enum ElectroState
	{
		ON		  = 0,
		FAULT	  = 8,
		RUNNING = 10,
		ALARM	  = 11,
		UNKNOWN	= 13
	};
	
	ElectroState electrometerState;
	
	void set_electroState(ElectroState newState) { electrometerState = newState; }

	short			_range;
	std::string		_rangeStr;
	std::string		_mode;
	std::string		_device_proxy_name;
	ElectrometerProtocol*	_electrometerProtocol;

	//- for internal use -> to configure the Integration mode
	short _size;
	short _trigMod;
private :

public :
	ElectroState electrometer_state	(void) { return electrometerState; }

};

#endif // _ABSTRACT_ELECTROMETER_CLASS_H_
