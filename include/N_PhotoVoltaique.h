// ============================================================================
//
// = CONTEXT
//    TANGO Project - Novelec Electrometer Support Library
//
// = FILENAME
//    N_PhotoVoltaique.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _NOVELEC_PHOTOVOLTAIQUE_H_
#define _NOVELEC_PHOTOVOLTAIQUE_H_

#include "Novelec_MCCE2.h"

/**
 *  \addtogroup Novelec
 *  @{
 */

/**
 *  \brief This class manage Novelec PhotoVoltaique types
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class N_PhotoVoltaique : public Novelec_MCCE2
{
public:

	/**
	*  \brief Initialization.
	*/
//	N_PhotoVoltaique (std::string comLink_device_name, short channel_address, short electroType);
	N_PhotoVoltaique (std::string comLink_device_name, short channel_address, std::string comProtocol);

	/**
	*  \brief Release resources.
	*/
	virtual ~N_PhotoVoltaique (void);

	/**
	*  \brief device dependent commands.
	*/
	void range_up	(void);
	void range_down (void);

	/**
	*  \brief Checks and Sets the new range value.
	*/
	void set_ElectroMeterRange(std::string range_str);
	std::string get_ElectroMeterRange();
	std::vector<std::string> ranges_list(void);

	/**
	*  \brief getters and setters.
	*/
	std::string get_ElectroMeterFrequency(void);
	void set_ElectroMeterFrequency	(std::string);

	/**
	* Electrometer Status.
	*/
//	std::string electrometer_status		(void);

protected :

};

/** @} */	//- end addtogroup

#endif // _NOVELEC_PHOTOVOLTAIQUE_H_
