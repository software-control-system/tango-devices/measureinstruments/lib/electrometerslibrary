// ============================================================================
//
// = CONTEXT
//    TANGO Project - Novelec Electrometer Support Library
//
// = FILENAME
//    Novelec_MCCE2.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _NOVELEC_MCCE2_H_
#define _NOVELEC_MCCE2_H_

#include "AbstractElectrometerClass.h"

/**
 *  \addtogroup Novelec
 *  @{
 */

/**
 *  \brief This class manage Novelec MCCE-2 type
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class Novelec_MCCE2 : public AbstractElectrometerClass
{
public:

	/**
	*  \brief Initialization.
	*/
//	Novelec_MCCE2 (std::string& comLink_device_name, unsigned short channel_address, unsigned short electroType);
	Novelec_MCCE2 (std::string& comLink_device_name, unsigned short channel_address, std::string comProtocol);

	/**
	*  \brief Release resources.
	*/
	virtual ~Novelec_MCCE2 (void);

  bool init_protocol(void);

	std::string get_ElectroMeterPolarity(void);
	void set_ElectroMeterPolarity	(std::string);

	short get_electrometer_type() {
    return this->_MCCE2electroTypeNumber;
  }
	std::string get_electrometer_typeStr() {
    return this->_MCCE2electroTypeStr;
  }

	/**
	*  \brief Electrometer Functions : mode for novelec types.
	*/
	void set_Zero_VonF_function			  (void);
	void set_Offset_ZeroV1_function		(void);
	void set_Leakage_ZeroV2_function	(void);
	void set_Test_function				    (void);
	void set_Measure_function			    (void);

	/**
	*	\brief Sets the new range value
	*/
	//virtual void set_ElectroMeterRange(std::string range_str) = 0;
  virtual void range_up	     (void) {};
  virtual void range_down    (void) {};

	/**
	*  \brief Commands to start/stop the data acquisition
	*
	*
	*	This emulate the MCCE2 key ON/OFF
	*	(all parameters must be set before switching ON the MCCE2)
	*/
	void mcce_on	(void);
	void mcce_off	(void);

	/**
	*  \brief Electrometer Status.
	*/
	std::string electrometer_status		(void);

  virtual std::vector<std::string> ranges_list(void) {
  	return std::vector<std::string>() ;
  };


protected :
	unsigned short _rangeLimit;				      //- define the range limit for each novelec type
  unsigned short _address;                //- a DAIP manages 2 "channels" (or address)
	unsigned short _MCCE2electroTypeNumber;	//- the mcce2 electrometer type number (to check range value)
    std::string _MCCE2electroTypeStr;       //-  "      "   "   "   "   "   as string
	/**
	*	\brief Checks the new range value
	*/
	short check_range_value(const std::string& rgToCheck, const std::vector<std::string>	 electroRangeList);
private:
	std::string _comProtocol;

};

/** @} */	//- end addtogroup

#endif // _NOVELEC_MCCE2_H_
