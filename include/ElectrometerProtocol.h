// ============================================================================
//
// = CONTEXT
//    TANGO Project - Electrometer Support Library
//
// = FILENAME
//    ElectrometerProtocol.h
//
// = AUTHOR
//    X. Elattaoui
//
//
// $Author: xavela $
//
// $Revision: 1.15 $
//
// $Log: not supported by cvs2svn $
// Revision 1.14  2011/03/14 14:42:22  xavela
// SaveRestoreConfiguration command added. (Available only for SCPI Keithley devices)
//
// Revision 1.13  2010/06/10 14:36:15  xavela
// cmd switch_off called before any parameters modifications and then locked with switch_on.
//
// Revision 1.12  2009/10/14 15:26:17  xavela
// KeithleyMemory leak fixed.
// Novelec part updated
//
// Revision 1.11  2009/01/19 12:00:25  xavela
// xavier :
// - MCCE2 (Novelec part) : methods to change the active channel to communicate with.
//
// Revision 1.10  2008/06/20 14:36:24  xavela
// xavier :
// - DDC status show only errors if any
// - added command to get DDC model configuration
//
// Revision 1.9  2008/04/15 12:52:00  xavela
// xavier :
// - SRQ management changed :
// * ReadStatusByteRegister method added
// * IsSrqLineUP added
//
// Revision 1.8  2008/02/15 10:17:55  xavela
// xavier :
// - command abort added for SCPI Keithleys
//
// Revision 1.7  2008/02/15 10:15:16  xavela
// xavier :
// - command abort added for SCPI Keithleys
//
// Revision 1.6  2008/02/15 08:49:46  xavela
// xavier :
// - minor change for integration time
//
// Revision 1.5  2008/02/13 15:51:44  xavela
// xavier :
// - Integration Mode available for DDC/SCPI devices
// - tests done with a K617 (DDC) and a K6485 (SCPI)
//
// Revision 1.4  2008/02/11 16:55:04  xavela
// xavier : for DDC part
// - integration mode OK (tests done with K_486)
//
// - TODO: DDC/SCPI
// integration time for ScanServer compatibility.
// report config for all DDC
// add configuration in SCPI start command
//
// Revision 1.3  2008/02/08 17:24:32  xavela
// xavier : for DDC part
// - trigger mode and buffer size management added.
// - TODO: DDC/SCPI
// integration time for ScanServer compatibility.
// SRQ management for Keithley Integration mode
//
// Revision 1.2  2007/11/27 10:17:46  xavela
// xavier :
// - modif done on Novelec part :
// init_MCCE2() added
//
// Revision 1.1  2007/07/09 13:20:35  stephle
// initial import
//
//
// ============================================================================

#ifndef _ELECTROMETER_PROTOCOL_H_
#define _ELECTROMETER_PROTOCOL_H_

#include <vector>
//#include "CommunicationLink.h"

/**
 *  \addtogroup Standard Commands Protocol 
 *  @{
 */

/**
 *  \brief This abstract class manage the Electrometers Commands Protocol
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class ElectrometerProtocol
{
public:

	/**
	*  \brief Initialization. 
	*/
	ElectrometerProtocol ();
	
	/**
	*  \brief Release resources.
	*/
	virtual ~ElectrometerProtocol ();

  
  /**
  *  \brief create the communication device proxy.
  */
  virtual bool build_communicationLink() = 0;

	/**
	*  \brief Functions to save/restore specifics configuration.
	*		NOTE : only available fro SCPI devices
	*/
	virtual void save_configuration(unsigned short memoryIdx);
	virtual void restore_configuration(unsigned short memoryIdx);

	/**
	*  \brief Common Electrometer Functions.
	*/
    virtual void set_range (std::string val)= 0;
    virtual void set_range (std::size_t val);
	virtual void reset			(void) = 0; 
	virtual void local			(void); 
	virtual void remote			(void); 

	/**
	*  \brief Common Electrometer : getters & setters.
	*/
	virtual std::string get_value(void)		= 0;
	virtual std::string get_mode (void)		= 0;
    virtual std::string get_range(void)     = 0;
	virtual std::vector<double> get_integratedValue		(void);
	virtual std::vector<double> get_fetchValue		    (void);
	virtual bool SRQLineState             (void); //- used to know if the integration cycle is done! 
	virtual short readStatusByteRegister  (void); //- device status byte register value on SRQ! 
	virtual bool get_overloadRangeState   (void); //- used to know if the device range is overloaded! 

	/**
	*  \brief Common Get Raw Electrometer Status.
	*/
	virtual std::string get_raw_status (void) = 0;
	
	/**
	*  \brief Returns the DDC Keithley configuration
	*			asking its "Machine Status" word (= Using "U0X" cmd) .
	*/
	virtual std::string get_DDC_configuration	(void);

	/**
	*  \brief Electrometer : Keithley protocol dependent commands.
	*/
	virtual void init_keithley		(void); 
	virtual void abort        		(void); 
	virtual void set_knplc			  (std::string nPLC);
	virtual void set_triggercount	(std::string trigcounts);
	virtual void set_triggerdelay	(std::string trigdelay);
	virtual void set_triggerMode  (std::string);
	virtual void set_triggerdelayAuto (std::string trigdelAuto);
	virtual void set_averagecount     (std::string avercounts);
	virtual void set_averagecontrol   (std::string averctrl);
	virtual std::string get_knplc			(void);
	virtual std::string get_triggercount	(void);
	virtual std::string get_triggerdelay	(void);
	virtual std::string get_triggerdelayAuto(void);
	virtual std::string get_averagecount	  (void);
	virtual std::string get_averagecontrol	(void);
	virtual void clear_registers	(void); 
	virtual void averageStateON		(void); 
	virtual void averageStateOFF	(void); 
	virtual void setAmperMeterMode(void);
	virtual void setVoltMeterMode	(void);
	virtual void setOhmMeterMode	(void);
	virtual void setCoulombMeterMode(void);
	virtual void autoRange_on		  (void);
	virtual void autoRange_off		(void);
	virtual std::string is_autoRange_on  (void);
	virtual void zero_check_on		(void);
	virtual void zero_check_off		(void);
	virtual void zero_correct_on	(void);
	virtual void zero_correct_off	(void);
	virtual void auto_zero_on		  (void);
	virtual void auto_zero_off		(void);

	/**
	*  \brief Electrometer : Integration Mode configuration
	*/
	virtual void set_buffer_size		(std::string size);
	virtual void clear_buffer		    (void);
	virtual void store_raw_input		(void);
	virtual void start_storing			(void);
	virtual void enable_SRQBufferFull	(void);
	virtual void disable_SRQBufferFull	(void);

	virtual void set_conversionRate                 (void);
	virtual void enable_readingWithPrefix           (void);
	virtual void disable_readingWithPrefix          (void);
	virtual void enable_ReadingsFromElectrometer    (void);
	virtual void enable_readingsFromBuffer_K617_6512(void);
	virtual void enable_readingsFromBuffer_K486_487 (void);
	virtual void read_data_with_no_timestamp        (void);

	virtual std::string get_buffer_size	 (void);

	//- only for K_486 and K_487 devices
	virtual void enable_integrationPeriod(void);


	/**
	*  \brief Electrometer : Novelec protocol dependent commands.
	*/
	virtual void switch_MCCE2_ON		        (void);
	virtual void switch_MCCE2_OFF		        (void);
	virtual void init_MCCE2_for_communication(void);
	virtual void unable_zeroVF_func		      (void);
	virtual void unable_offset_zeroV1_func	(void);
	virtual void unable_leakage_zeroV2_func	(void);
	virtual void unable_test_func	          (void);
	virtual void unable_measure_func        (void);
	virtual std::string get_polarity	      (void);
	virtual std::string get_frequency	      (void);
	virtual std::string get_gain(void);
	virtual void set_polarity		(std::string pola);
	virtual void set_frequency	(std::string freq);
	virtual void set_gain			  (std::string gain);
	virtual unsigned short get_electrometer_active_channel(void);
	virtual void set_electrometer_active_channel(unsigned short address);



protected :
	short		    _range;
	std::string	_mode;
  std::string _commDevName;   //- communication device proxy name

private :


};

/** @} */	//- end addtogroup

#endif // _ELECTROMETER_PROTOCOL_H_
