// ============================================================================
//
// = CONTEXT
//    TANGO Project - DDC Keithley Electrometer Support Library
//
// = FILENAME
//    Keithley_617.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _KEITHLEY_617_H_
#define _KEITHLEY_617_H_

#include "AbstractElectrometerClass.h"

/**
 *  \addtogroup DDC Keithley
 *  @{
 */

/**
 *  \brief This class manage 617 Keithley type
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class Keithley_617 : public AbstractElectrometerClass
{
public:

	/**
	*  \brief Initialization. 
	*/
	Keithley_617 (std::string& comLink_device_name);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~Keithley_617 (void);

	/**
	*  \brief protocole initailisation.
	*/
  bool init_protocol (void);

	/**
	*  \brief Device dependent commands.
	*/
	void autoRange_off		(void);
	void range_up			(void);
	void range_down			(void);
	void set_buffer_size	(short not_used);
	void set_triggerMode	(short);
	void init_keithley		(void); 
	
	/**
	*  \brief Electrometer Mode.
	*/
	void setAmperMeterMode  (void);
	void setVoltMeterMode   (void);
	void setOhmMeterMode    (void);
	void setCoulombMeterMode(void);

	/**
	*  \brief getter(s) & setter(s)	
	*/
	std::string get_ElectroMeterMode (void);
	std::string get_ElectroMeterRange(void);
    std::vector<std::string> ranges_list(void);
    void set_ElectroMeterRange (std::size_t rangeIdx);

  /**
	*  \brief Electrometer status.
	*/
	std::string electrometer_status (void);

	/**
	*  \brief Returns the DDC Keithley configuration
	*			asking its "Machine Status" word (= Using "U0X" cmd) .
	*/
	std::string get_configuration	(void);
	
	//-	TODO :
	//	DDC_Filters*	_ddcFilters;
	//	DDC_Triggers*	_ddcTriggers;
	
};

/** @} */	//- end addtogroup

#endif // _KEITHLEY_617_H_
