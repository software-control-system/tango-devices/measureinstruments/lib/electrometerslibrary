// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Electrometer Support Library
//
// = FILENAME
//    Keithley_485.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _KEITHLEY_485_H_
#define _KEITHLEY_485_H_

#include "AbstractElectrometerClass.h"

/**
 *  \addtogroup DDC_Keithley 
 *  @{
 */

/**
 *  \brief This class manage 485 Keithley type
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class Keithley_485 : public AbstractElectrometerClass
{
public:

	/**
	*  \brief Initialization. 
	*/
	Keithley_485 (std::string& comLink_device_name);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~Keithley_485 (void);

	/**
	*  \brief protocole initailisation.
	*/
  bool init_protocol (void);

	/**
	*  \brief Device dependent commands.
	*/
	void range_up		  (void);
	void range_down		(void);

  /**
	*  \brief Trigger Mode Command
	*/
  void set_triggerMode  (short);

	/**
	*  \brief getter(s) & setter(s)	
	*/
	std::string get_ElectroMeterRange(void);
    std::vector<std::string> ranges_list(void);
    void set_ElectroMeterRange (std::size_t rangeIdx);
	
	/**
	*  \brief Electrometer Status.
	*/
	std::string electrometer_status (void);
};

/** @} */	//- end addtogroup

#endif // _KEITHLEY_485_H_
