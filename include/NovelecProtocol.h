// ============================================================================
//
// = CONTEXT
//    TANGO Project - Novelec Electrometer Support Library
//		( Supported type : MCCE-2 )
// = FILENAME
//    NovelecProtocol.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _NOVELEC_MCCE2_PROTOCOL_H_
#define _NOVELEC_MCCE2_PROTOCOL_H_

#include "ElectrometerProtocol.h"
#include "ElectrometerException.h"
#include "TangoSerialLink.h"
#include "TangoEthernetLink.h"

/**
 *  \addtogroup Standard Commands Protocol
 *  @{
 */

/**
 *  \brief This class manage the Novelec Commands Protocol
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class NovelecProtocol : public ElectrometerProtocol
{
public:

	/**
	*  \brief Initialization.
	*/
	//NovelecProtocol (std::string& communication_device_name, unsigned short channel_address, unsigned short electroTypeNumber);
	NovelecProtocol (std::string& communication_device_name, unsigned short channel_address, std::string comProtocol);

	/**
	*  \brief Release resources.
	*/
	virtual ~NovelecProtocol(void);

	/**
	*  \brief create the communication device proxy.
	*/
  bool build_communicationLink();

  /**
	*  \brief Electrometer Functions.
	*/
	void set_range	(std::string value);
	std::string get_range		(void);

	void reset (void);
	void local (void);
	void remote(void);
	void clear_registers(void);			//- clear all errors

	/**
	*  \brief Electrometer : cmd to get electrometer data.
	*/
	std::string get_value		(void);

	/**
	*  \brief Electrometer Mode : Novelec SUPPORTS these commands
	*/
	void switch_MCCE2_ON		        (void);
	void switch_MCCE2_OFF		        (void);
	void unable_zeroVF_func		      (void);
	void unable_offset_zeroV1_func	(void);
	void unable_leakage_zeroV2_func	(void);
	void unable_test_func		        (void);
	void unable_measure_func	      (void);

	/**
	*  \brief Get Raw Electrometer Status.
	*/
	std::string get_raw_status (void) ;
	/**
	*  \brief Electrometer Mode : getters and setters.
	*/
	std::string get_mode		  (void);
	std::string get_polarity	(void);
	std::string get_frequency	(void);
	std::string get_gain		  (void);
	std::string get_errors		(void);
	short get_electrometerType(void) {
	  return this->_novType;
	}
	void set_polarity	  (std::string pola);
	void set_frequency	(std::string freq);
	void set_gain		    (std::string gain);
	void set_electrometer_active_channel(unsigned short channel_add) {
	  this->_devAdd = channel_add;
  }
	unsigned short get_electrometer_active_channel(void) {
	  return this->_devAdd;
  }

	/**
	*   \brief init the MCCE-2 device :
	*        - response explicit
	*        - mode MEASURE OFF
	*        - clear error(s)
	*/
	void init_MCCE2_for_communication(void);
  std::string check_electrotype (void);

protected :
	short _devAdd;								//- novelec active channel address
	short _novType;								//- novelec type number (1 to 5)
	short _rangeParameterNum;			//-	command number for the range
	std::string _function;				//- the novelec mode

	//- check if the cmd is understood
	void check_command	(std::string resp_to_check);
	//- check if the cmd is understood and extract data from the response
	//- To do so, it check if the command number sent is in the received response (just after AWR cmdNum)
	//- reply format : "devAddr AWR cmdNum = response " exp : response status "01 AWR 01 = value"
	std::string check_and_extract_data	(std::string resp_to_check, short command_sent);

private :
  //- check if the MCCE2 is not in MEASURE mode to get/change any settings
	void is_allowed();

	CommunicationLink* _communication_link;
	//communication protocol ETHERNET/SERIAL
	std::string _comProtocol;

	//- if true, parameters cannot be changed !
	bool	_is_measure_mode_on;
	//- if true, explicite response mode is enabled
	bool	_is_explicite_resp_enabled;
  //- the specific new line character
  string END_OF_LINE ;

};

/** @} */	//- end addtogroup

#endif // _NOVELEC_MCCE2_PROTOCOL_H_
