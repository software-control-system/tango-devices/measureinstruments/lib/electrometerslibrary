// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Electrometer Support Library
//
// = FILENAME
//    Keithley_6485.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _KEITHLEY_6485_H_
#define _KEITHLEY_6485_H_

#include "AbstractElectrometerClass.h"

/**
 *  \addtogroup SCPI Keithley
 *  @{
 */

/**
 *  \brief This class manage 6485 Keithley type
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class Keithley_6485 : public AbstractElectrometerClass
{
public:

	/**
	*  \brief Initialization. 
	*/
	Keithley_6485 (std::string& comLink_device_name);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~Keithley_6485 (void);

	/**
	*  \brief protocole initailisation.
	*/
  bool init_protocol (void);

	/**
	*  \brief Functions to save/restore specifics configuration.
	*/
	void save_configuration(unsigned short memoryIdx);
	void restore_configuration(unsigned short memoryIdx);

  std::vector<double> get_integratedValue (void);
	std::vector<double> get_fetchValue      (void);
	/**
	*  \brief The integration time (sec).
	*/
	void set_integrationTime			(double);

	/**
	*  \brief Device dependent commands.
	*/
	void range_up   (void);
	void range_down (void);
  std::string get_ElectroMeterRange (void);
    std::vector<std::string> ranges_list(void);
    void set_ElectroMeterRange (std::size_t rangeIdx);

	/**
	*  \brief Methods to configure the integration mode.
	*/
  void set_knplc        (double);
  void set_triggercount (short);
  void set_buffer_size  (short);
	void init_keithley		(void); 

private:
	//- method to obtain the range index
	void update_range (void);
	
  double  _numPLC;
  int  _trigCounts;
};

/** @} */	//- end addtogroup

#endif // _KEITHLEY_6485_H_
