// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Electrometer Support Library
//
// = FILENAME
//    TangoGpibLink.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _TANGO_GPIB_LINK_H_
#define _TANGO_GPIB_LINK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include "CommunicationLink.h"

/**
 *  \addtogroup Communication Management
 *  @{
 */

/**
 *  \brief This class manage the GPIB communication bus
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class TangoGpibLink : public CommunicationLink
{

public :
	/**
	* Initialization.
	*/
	TangoGpibLink (std::string& gpib_device_name);

	/**
	*  \brief Release resources.
	*/
	virtual ~TangoGpibLink (void);

	/**
	*  \brief Send command (data) as string to hardware.
	*
	*  \throws Tango::DevFailed
	*/
	void write(std::string cmd)	 throw (Tango::DevFailed);

	/**
	*  \brief Gets hardware response as string.
	*
	*  \throws Tango::DevFailed
	*/
	std::string read(void)		 throw (Tango::DevFailed);

	/**
	*  \brief Performs a write read operation as string.
	*
	*  \throws Tango::DevFailed
	*/
	std::string write_read(std::string cmd, size_t readLength=0)  throw (Tango::DevFailed);

	/**
	*  \brief Returns the Gpib SRQ line state : used here to know if Keithley
  *           device is ready after an integration cycle.
	*
	*  \throws Tango::DevFailed
	*/
	bool SRQLineState             (void) throw (Tango::DevFailed); //- used to know if the integration cycle is done!
	short readStatusByteRegister  (void) throw (Tango::DevFailed); //- device status byte register value on SRQ!

	/**
	*  \brief Clear a specific device (same as reset *RST).
	*
	*  \throws Tango::DevFailed
	*/
	void clear(void)  throw (Tango::DevFailed);

	/**
	*  \brief Trigg device on the gpib bus.
	*
	*  \throws Tango::DevFailed
	*/
	void trigger(void)  throw (Tango::DevFailed);

private :

	/**
	* Creates a proxy on the specified GPIB Device.
	*/
	void create_gpib_proxy(void) throw (Tango::DevFailed);

	Tango::DeviceProxy* _gpib_proxy;

};

/** @} */	//- end addtogroup

#endif		// _TANGO_GPIB_LINK_H_
