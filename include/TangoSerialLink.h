// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Electrometer Support Library
//
// = FILENAME
//    TangoSerialLink.h.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _TANGO_SERIAL_LINK_H_
#define _TANGO_SERIAL_LINK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include "CommunicationLink.h"

/**
 *  \addtogroup Communication Management
 *  @{
 */

/**
 *  \brief This class manage the SERIAL communication bus
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class TangoSerialLink : public CommunicationLink
{

public :
 	/**
	* \brief Initialization.
	*/
	TangoSerialLink (std::string& serial_device_name);

	/**
	* \brief Release resources.
	*/
	~TangoSerialLink ();

	/**
	*  \brief Send command (data) as string to hardware.
	*
	*  \throws Tango::DevFailed
	*/
	void write(std::string cmd)	 throw (Tango::DevFailed);

	/**
	*  \brief Gets hardware response as string. (mode LINE)
	*
	*  \throws Tango::DevFailed
	*/
	std::string read(void)		 throw (Tango::DevFailed);

	/**
	*  \brief Gets hardware response as string. (read nbCharToRead)
	*
	*  \throws Tango::DevFailed
	*/
	std::string read(long nbCharToRead)		 throw (Tango::DevFailed);

	/**
	*  \brief Performs a write read operation as string. (read nb char)
	*
	*  \throws Tango::DevFailed
	*/
	std::string write_read(std::string cmd, size_t nbChar)  throw (Tango::DevFailed);

private :

  /**
	* Creates a proxy on the specified Serial Device.
	*/
	void check_proxy(void) throw (Tango::DevFailed);

	Tango::DeviceProxy* _serial_proxy;

	bool _is_serial_proxy_ok;

};

/** @} */	//- end addtogroup

#endif // _TANGO_SERIAL_LINK_H_
