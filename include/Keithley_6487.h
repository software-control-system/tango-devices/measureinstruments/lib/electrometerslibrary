// ============================================================================
//
// = CONTEXT
//    TANGO Project - SCPI Keithley Electrometer Support Library
//
// = FILENAME
//    Keithley_6487.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _KEITHLEY_6487_H_
#define _KEITHLEY_6487_H_

#include "AbstractElectrometerClass.h"

/**
 *  \addtogroup SCPI Keithley
 *  @{
 */

/**
 *  \brief This class manage 6487 Keithley type
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class Keithley_6487 : public AbstractElectrometerClass
{
public:

	/**
	*  \brief Initialization. 
	*/
	Keithley_6487 (std::string& comLink_device_name);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~Keithley_6487 (void);

	/**
	*  \brief protocole initailisation.
	*/
  bool init_protocol (void);

	/**
	*  \brief Functions to save/restore specifics configuration.
	*/
	void save_configuration(unsigned short memoryIdx);
	void restore_configuration(unsigned short memoryIdx);

	/**
	*  \brief Device dependent commands.
	*/
	void range_up (void);
	void range_down (void);
  std::string get_ElectroMeterRange (void);
    std::vector<std::string> ranges_list(void);
    void set_ElectroMeterRange (std::size_t rangeIdx);
	
  std::vector<double> get_integratedValue (void);
	std::vector<double> get_fetchValue      (void);
	/**
	*  \brief The integration time (sec).
	*/
	void set_integrationTime			(double);

	/**
	*  \brief Electrometer Mode.
	*/
	void setAmperMeterMode (void);

	/**
	*  \brief Methods to configure the integration mode.
	*/
  void set_knplc        (double);
  void set_triggercount (short);
  void set_buffer_size  (short);
	void init_keithley		(void); 

private:
	//- method to obtain the range index
	void update_range (void);
	
  double  _numPLC;
  int  _trigCounts;
};

/** @} */	//- end addtogroup

#endif // _KEITHLEY_6487_H_
