//******************************************************************************************
//
//
//		july 24, 2006 :  Header file for the Electrometers exceptions
//					     (avaiable for all models Keithley and Novelec)
//
//
//		author : X.Elattaoui
//
//		ElectrometerException.cpp: interface for the electrometer exceptions class.
//
//******************************************************************************************

#ifndef _ELECTROMETER_EXCEPTION
#define _ELECTROMETER_EXCEPTION

#include <string>
#include <vector>

namespace electrometer{

// ============================================================================
// Electrometer Errors severities
// ============================================================================
typedef enum {
  WARN, 
  ERR, 
  PANIC
} ErrorSeverity;

// ============================================================================
//! The Electrometer exception abstraction base class.
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class Error
{
public:

  /**
   * Initialization. 
   */
  Error (void);

  /**
   * Initialization. 
   */
  Error (const char *reason,
				 const char *desc,
				 const char *origin,
	       int severity = electrometer::ERR);
  

  /**
   * Initialization. 
   */
  Error (const std::string& reason,
				 const std::string& desc,
				 const std::string& origin, 
	       int severity = electrometer::ERR);

  /**
   * Copy constructor. 
   */
  Error (const Error& src);

  /**
   * Error details: code 
   */
  virtual ~Error (void);

  /**
   * operator= 
   */
  Error& operator= (const Error& _src);

  /**
   * Error details: reason 
   */
  std::string reason;

  /**
   * Error details: description 
   */
  std::string desc;

  /**
   * Error details: origin 
   */
  std::string origin;

  /**
   * Error details: severity 
   */
  int severity;

};

// ============================================================================
// The Electrometer error list.
// ============================================================================
typedef std::vector<Error> ErrorList;


// ============================================================================
//! The Electrometer exception abstraction base class.
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class  ElectrometerException
{
public:

  /**
   * Initialization. 
   */
  ElectrometerException (void);

  /**
   * Initialization. 
   */
  ElectrometerException (const char *reason,
					   const char *desc,
					   const char *origin,
		   int severity = electrometer::ERR);
  
  /**
   * Initialization. 
   */
  ElectrometerException (const std::string& reason,
					   const std::string& desc,
					   const std::string& origin, 
		   int severity = electrometer::ERR);

  /**
   * Initialization. 
   */
  ElectrometerException (const Error& error);


  /**
   * Copy constructor. 
   */
  ElectrometerException (const ElectrometerException& src);

  /**
   * operator=
   */
  ElectrometerException& operator= (const ElectrometerException& _src);

  /**
   * Release resources.
   */
  virtual ~ElectrometerException (void);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const char *reason,
						 const char *desc,
						       const char *origin, 
			       int severity = electrometer::ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const std::string& reason,
		   const std::string& desc,
		   const std::string& origin, 
		   int severity = electrometer::ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const Error& error);

  /**
   * The errors list
   */
   ErrorList errors;
  

};

} // end namspace Electrometer
#endif //_ELECTROMETER_EXCEPTION


