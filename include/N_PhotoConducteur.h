// ============================================================================
//
// = CONTEXT
//    TANGO Project - Novelec Electrometer Support Library
//
// = FILENAME
//    N_PhotoConducteur.h
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _NOVELEC_PHOTOCONDUCTEUR_H_
#define _NOVELEC_PHOTOCONDUCTEUR_H_

#include "Novelec_MCCE2.h"

/**
 *  \addtogroup Novelec
 *  @{
 */

/**
 *  \brief This class manage Novelec PhotoConductor types
 *
 *  \author Xavier Elattaoui
 *  \date 11-2006
 */

class N_PhotoConducteur : public Novelec_MCCE2
{
public:

	/**
	*  \brief Initialization.
	*/
//	N_PhotoConducteur (std::string& comLink_device_name, short channel_address, short electroType);
	N_PhotoConducteur (std::string& comLink_device_name, short channel_address, std::string comProtocol);

	/**
	*  \brief Release resources.
	*/
	virtual ~N_PhotoConducteur	(void);

	/**
	*  \brief device dependent commands.
	*/
	void	range_up			(void);
	void	range_down			(void);

	/**
	*  \brief Checks and Sets the new range value.
	*/
	void set_ElectroMeterRange(std::string range_str);
	std::string get_ElectroMeterRange();
	std::vector<std::string> ranges_list(void);

	/**
	*  \brief getters and setters.
	*/
	std::string get_ElectroMeterGain	(void);
	void	set_ElectroMeterGain		(std::string);

protected :

};

/** @} */	//- end addtogroup

#endif // _NOVELEC_PHOTOCONDUCTEUR_H_
