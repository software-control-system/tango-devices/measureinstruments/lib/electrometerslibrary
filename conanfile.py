from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMakeDeps

class electrometersRecipe(ConanFile):
    name = "electrometers"
    version = "2.8.1"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Xavier Elattaoui"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/measureinstruments/lib/electrometers"
    description = "Electrometers library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"

    def requirements(self):
        self.requires("cpptango/9.2.5@soleil/stable")
        self.requires("yat/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
