// ============================================================================
//
// = CONTEXT
//    TANGO Project - NovelecElectrometer Support Library
//			PhotoVoltaic Types are Type 1, 2 & 3
//
// = FILENAME
//    N_PhotoVoltaique.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <sstream>
#include <string>
#include "N_PhotoVoltaique.h"
#include "NovelecProtocol.h"
/*
* Valid Range values for a N_PhotoVoltaique
*/
static const std::vector<std::string> NType1_rangeValue {"1E-11AcC","3E-11AcC","1E-10AcC","3E-10AcC"};
static const std::vector<std::string> NType2_rangeValue {"1E-10AcC","3E-10AcC","1E-09AcC","3E-09AcC","1E-08AcC","3E-08AcC","1E-07AcC","3E-07AcC"};
static const std::vector<std::string> NType3_rangeValue {"1E-08AcC","3E-08AcC","1E-07AcC","3E-07AcC","1E-06AcC","3E-06AcC","1E-05AcC","3E-05AcC"};


// ============================================================================
// N_PhotoVoltaique::N_PhotoVoltaique
// ============================================================================
N_PhotoVoltaique::N_PhotoVoltaique (std::string comLink_device_name, short address, std::string comProtocol)
:	Novelec_MCCE2(comLink_device_name, address, comProtocol)
{
	std::cout << "N_PhotoVoltaique::N_PhotoVoltaique <-" << std::endl;

	//std::cout << "N_PhotoVoltaique::N_PhotoVoltaique ->" << std::endl;
}

// ============================================================================
// N_PhotoVoltaique::~N_PhotoVoltaique
// ============================================================================
N_PhotoVoltaique::~N_PhotoVoltaique (void)
{
	//std::cout << "N_PhotoVoltaique::~N_PhotoVoltaique <-" << std::endl;

	//std::cout << "N_PhotoVoltaique::~N_PhotoVoltaique ->" << std::endl;
}

// ============================================================================
// N_PhotoVoltaique::range_up
// ============================================================================
void N_PhotoVoltaique::range_up (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoVoltaique::range_up( ).");

std::stringstream cmd_to_send;

  //- update range idx
  this->get_ElectroMeterRange();

	_range += 1;
/*
	//- get range limit
	switch(_MCCE2electroTypeNumber)
	{
	case 1 :	_rangeLimit = 3;
		break;
	case 2 :
	case 3 :	_rangeLimit = 7;
		break;
	}
*/
	if(_range > _rangeLimit)
	{
		_range = _rangeLimit;
		throw electrometer::ElectrometerException("OUT_OF_RANGE",
												"Range up limit reached.",
												"N_PhotoVoltaique::range_up( ).");
	}

	//- build and send the command
	cmd_to_send << _range;
	_electrometerProtocol->set_range(cmd_to_send.str());
}

// ============================================================================
// N_PhotoVoltaique::range_down
// ============================================================================
void N_PhotoVoltaique::range_down (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoVoltaique::range_down( ).");

std::stringstream cmd_to_send;

  //- update range idx
  this->get_ElectroMeterRange();

	_range -= 1;
/*
	//- get range limit
	switch(_MCCE2electroTypeNumber)
	{
	case 1 :	_rangeLimit = 3;
		break;
	case 2 :
	case 3 :	_rangeLimit = 7;
		break;
	}
*/
	if(_range < 0)
	{
		_range = 0;
		throw electrometer::ElectrometerException("OUT_OF_RANGE",
												"Range down limit reached.",
												"N_PhotoVoltaique::range_down( ).");
	}

	//- build and send the command
//	_rangeStr = NType1_rangeValue[_range];
	cmd_to_send << _range;

	_electrometerProtocol->set_range(cmd_to_send.str());
}

// ============================================================================
// N_PhotoVoltaique::get_ElectroMeterFrequency Filter
// ============================================================================
std::string N_PhotoVoltaique::get_ElectroMeterFrequency (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoVoltaique::get_ElectroMeterFrequency( ).");

	return _electrometerProtocol->get_frequency();
}

// ============================================================================
// N_PhotoVoltaique::set_ElectroMeterFrequency Filter
// ============================================================================
void N_PhotoVoltaique::set_ElectroMeterFrequency (std::string freqFilter)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoVoltaique::set_ElectroMeterFrequency( ).");

  _electrometerProtocol->set_frequency(freqFilter);
}

// ============================================================================
// N_PhotoVoltaique::set_ElectroMeterRange
// ============================================================================
void N_PhotoVoltaique::set_ElectroMeterRange (std::string rgStr)
{
std::stringstream range_cmd_to_send;

  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoVoltaique::set_ElectroMeterRange( ).");

	//- switch the novelec type :
	switch(_MCCE2electroTypeNumber)
	{
	case 1 : _range = Novelec_MCCE2::check_range_value(rgStr, NType1_rangeValue);
		break;
	case 2 : _range = Novelec_MCCE2::check_range_value(rgStr, NType2_rangeValue);
		break;
	case 3 : _range = Novelec_MCCE2::check_range_value(rgStr, NType3_rangeValue);
		break;
	}

	if (_range < 0)
		throw electrometer::ElectrometerException("INVALID_PARAMETER",
												  "This electrometer does not support this range value.",
												  "N_PhotoVoltaique::set_ElectroMeterRange( ).");
	//- it is OK
	range_cmd_to_send << _range;

	_electrometerProtocol->set_range(range_cmd_to_send.str());
}

// ============================================================================
// N_PhotoVoltaique::get_ElectroMeterRange
// ============================================================================
std::string N_PhotoVoltaique::get_ElectroMeterRange()
{
std::string rangeStr("");

  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoVoltaique::get_ElectroMeterRange( ).");

	rangeStr = this->_electrometerProtocol->get_range();

	//- switch the novelec type :
	switch(_MCCE2electroTypeNumber)
	{
	case 1 : 
        _range = Novelec_MCCE2::check_range_value(rangeStr, NType1_rangeValue);
		break;
	case 2 : 
        _range = Novelec_MCCE2::check_range_value(rangeStr, NType2_rangeValue);
		break;
	case 3 :
        _range = Novelec_MCCE2::check_range_value(rangeStr, NType3_rangeValue);
		break;
	}

	if (_range < 0)
		throw electrometer::ElectrometerException("INVALID_PARAMETER",
												  "Cannot find the applied range value.",
												  "N_PhotoVoltaique::get_ElectroMeterRange( ).");

	return rangeStr;
}

// ============================================================================
// N_PhotoVoltaique::ranges_list
// ============================================================================
std::vector<std::string> N_PhotoVoltaique::ranges_list(void)
{
  std::vector<std::string> vrangeslist;

	//- switch the novelec type :
	switch(_MCCE2electroTypeNumber)
	{
	case 1 : vrangeslist = NType1_rangeValue;
		break;
	case 2 : vrangeslist = NType2_rangeValue;
		break;
	case 3 : vrangeslist = NType3_rangeValue;
		break;
	}
	return vrangeslist;
}
