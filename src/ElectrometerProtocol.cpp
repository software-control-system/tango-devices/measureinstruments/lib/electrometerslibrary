// ============================================================================
//
// = CONTEXT
//    TANGO Project - ElectrometerProtocol Support Library
//
// = FILENAME
//    ElectrometerProtocol.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include "ElectrometerProtocol.h"
#include "ElectrometerException.h"

// ============================================================================
// ElectrometerProtocol::ElectrometerProtocol
// ============================================================================
ElectrometerProtocol::ElectrometerProtocol () :
	_range(0),
	_mode("NOT INITIALISED"),
  _commDevName("")
{
	//std::cout << "ElectrometerProtocol::ElectrometerProtocol <-" << std::endl;

	//std::cout << "ElectrometerProtocol::ElectrometerProtocol ->" << std::endl;
}

// ============================================================================
// ElectrometerProtocol::~ElectrometerProtocol
// ============================================================================
ElectrometerProtocol::~ElectrometerProtocol (void)
{
	std::cout << "ElectrometerProtocol::~ElectrometerProtocol <-" << std::endl;

	std::cout << "ElectrometerProtocol::~ElectrometerProtocol ->" << std::endl;
}

// ============================================================================
// ElectrometerProtocol::local
// ============================================================================
void ElectrometerProtocol::local (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_IMPLEMENTED", 
											"This command is not implemented, if you want it post a bug in MANTIS.",
											"ElectrometerProtocol::local( ).");
}

// ============================================================================
// ElectrometerProtocol::remote
// ============================================================================
void ElectrometerProtocol::remote (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_IMPLEMENTED", 
											"This command is not implemented, if you want it post a bug in MANTIS.",
											"ElectrometerProtocol::remote( ).");
}

// ============================================================================
// ElectrometerProtocol::get_integratedValue
// ============================================================================
std::vector<double> ElectrometerProtocol::get_integratedValue (void) 
{
    throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
                                            "This Electrometer does not support this command.",
                                            "ElectrometerProtocol::get_integratedValue( ).");
}

// ============================================================================
// ElectrometerProtocol::set_range
// ============================================================================
void ElectrometerProtocol::set_range (std::size_t) 
{
    throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
                                            "This Electrometer does not support this command.",
                                            "ElectrometerProtocol::set_range( ).");
}

// ============================================================================
// ElectrometerProtocol::get_fetchValue
// ============================================================================
std::vector<double> ElectrometerProtocol::get_fetchValue (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_fetchValue( ).");
}

// ============================================================================
// ElectrometerProtocol::get_frequency
// ============================================================================
std::string ElectrometerProtocol::get_frequency (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_frequency( ).");
}

// ============================================================================
// ElectrometerProtocol::get_gain
// ============================================================================
std::string ElectrometerProtocol::get_gain (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_gain( ).");
}

// ============================================================================
// ElectrometerProtocol::get_polarity
// ============================================================================
std::string ElectrometerProtocol::get_polarity (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_polarity( ).");
}

// ============================================================================
// ElectrometerProtocol::set_polarity
// ============================================================================
void ElectrometerProtocol::set_polarity (std::string ) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_polarity( ).");
}

// ============================================================================
// ElectrometerProtocol::set_frequency
// ============================================================================
void ElectrometerProtocol::set_frequency (std::string ) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_frequency( ).");
}

// ============================================================================
// ElectrometerProtocol::set_gain
// ============================================================================
void ElectrometerProtocol::set_gain (std::string ) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_gain( ).");
}

// ============================================================================
// ElectrometerProtocol::unable_zeroVF_func
// ============================================================================
void ElectrometerProtocol::unable_zeroVF_func (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::unable_zeroVF_func( ).");
}

// ============================================================================
// ElectrometerProtocol::unable_offset_zeroV1_func
// ============================================================================
void ElectrometerProtocol::unable_offset_zeroV1_func (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::unable_offset_zeroV1_func( ).");
}

// ============================================================================
// ElectrometerProtocol::unable_leakage_zeroV2_func
// ============================================================================
void ElectrometerProtocol::unable_leakage_zeroV2_func (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::unable_leakage_zeroV2_func( ).");
}

// ============================================================================
// ElectrometerProtocol::unable_test_func
// ============================================================================
void ElectrometerProtocol::unable_test_func (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::unable_test_func( ).");
}

// ============================================================================
// ElectrometerProtocol::unable_measure_func
// ============================================================================
void ElectrometerProtocol::unable_measure_func (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::unable_measure_func( ).");
}

// ============================================================================
// ElectrometerProtocol::switch_MCCE2_ON
// ============================================================================
void ElectrometerProtocol::switch_MCCE2_ON (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::switch_MCCE2_ON( ).");
}

// ============================================================================
// ElectrometerProtocol::switch_MCCE2_OFF
// ============================================================================
void ElectrometerProtocol::switch_MCCE2_OFF (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::switch_MCCE2_OFF( ).");
}

// ============================================================================
// ElectrometerProtocol::init_MCCE2_for_communication
// ============================================================================
void ElectrometerProtocol::init_MCCE2_for_communication (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::init_MCCE2_for_communication( ).");
}

// ============================================================================
// ElectrometerProtocol::autoRange_ON
// ============================================================================
void ElectrometerProtocol::autoRange_on (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"Novelec does not support this command.",
											"ElectrometerProtocol::autoRange_on( ).");
}

// ============================================================================
// ElectrometerProtocol::autoRange_OFF
// ============================================================================
void ElectrometerProtocol::autoRange_off (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"Novelec does not support this command.",
											"ElectrometerProtocol::autoRange_off( ).");
}

// ============================================================================
// ElectrometerProtocol::is_autoRange_on
// ============================================================================
std::string ElectrometerProtocol::is_autoRange_on (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"Novelec does not support this command.",
											"ElectrometerProtocol::is_autoRange_on( ).");
}


// ============================================================================
// ElectrometerProtocol::zero_check_on
// ============================================================================
void ElectrometerProtocol::zero_check_on (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_IMPLEMENTED", 
											"This will be done soon !!!.",
											"ElectrometerProtocol::zero_check_on( ).");

}
// ============================================================================
// ElectrometerProtocol::zero_check_off
// ============================================================================
void ElectrometerProtocol::zero_check_off (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_IMPLEMENTED", 
											"This will be done soon !!!.",
											"ElectrometerProtocol::zero_check_off( ).");
}


// ============================================================================
// ElectrometerProtocol::zero_correct_on
// ============================================================================
void ElectrometerProtocol::zero_correct_on (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_IMPLEMENTED", 
											"This will be done soon !!!.",
											"ElectrometerProtocol::zero_correct_on( ).");
}

// ============================================================================
// ElectrometerProtocol::zero_correct_off
// ============================================================================
void ElectrometerProtocol::zero_correct_off (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_IMPLEMENTED", 
											"This will be done soon !!!.",
											"ElectrometerProtocol::zero_correct_off( ).");

}

// ============================================================================
// ElectrometerProtocol::auto_zero_on
// ============================================================================
void ElectrometerProtocol::auto_zero_on (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"Novelec does not support this command.",
											"ElectrometerProtocol::auto_zero_on( ).");

}

// ============================================================================
// ElectrometerProtocol::auto_zero_off
// ============================================================================
void ElectrometerProtocol::auto_zero_off (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"Novelec does not support this command.",
											"ElectrometerProtocol::auto_zero_off( ).");

}

// ============================================================================
// ElectrometerProtocol::setAmperMeterMode 
// ============================================================================
void ElectrometerProtocol::setAmperMeterMode (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"Novelec does not support this command.",
											"ElectrometerProtocol::setAmperMeterMode( ).");
}
// ============================================================================
// ElectrometerProtocol::setVoltMeterMode 
// ============================================================================
void ElectrometerProtocol::setVoltMeterMode (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"Novelec does not support this command.",
											"ElectrometerProtocol::setVoltMeterMode( ).");
}
// ============================================================================
// ElectrometerProtocol::setOhmMeterMode 
// ============================================================================
void ElectrometerProtocol::setOhmMeterMode (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"Novelec does not support this command.",
											"ElectrometerProtocol::setOhmMeterMode( ).");
}
// ============================================================================
// ElectrometerProtocol::setCoulombMeterMode 
// ============================================================================
void ElectrometerProtocol::setCoulombMeterMode (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"Novelec does not support this command.",
											"ElectrometerProtocol::setCoulombMeterMode( ).");
}

// ============================================================================
// ElectrometerProtocol::init_keithley
// ============================================================================
void ElectrometerProtocol::init_keithley (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::init_keithley( ).");
}

// ============================================================================
// ElectrometerProtocol::abort
// ============================================================================
void ElectrometerProtocol::abort (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::abort( ).");
}

// ============================================================================
// ElectrometerProtocol::set_knplc
// ============================================================================
void ElectrometerProtocol::set_knplc (std::string) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_knplc( ).");
}

// ============================================================================
// ElectrometerProtocol::get_knplc
// ============================================================================
std::string ElectrometerProtocol::get_knplc (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_knplc( ).");
}

// ============================================================================
// ElectrometerProtocol::set_triggerMode
// ============================================================================
void ElectrometerProtocol::set_triggerMode (std::string) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_triggerMode( ).");
}

// ============================================================================
// ElectrometerProtocol::set_triggercount
// ============================================================================
void ElectrometerProtocol::set_triggercount (std::string) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_triggercount( ).");
}

// ============================================================================
// ElectrometerProtocol::get_triggercount
// ============================================================================
std::string ElectrometerProtocol::get_triggercount (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_triggercount( ).");
}

// ============================================================================
// ElectrometerProtocol::set_triggerdelay
// ============================================================================
void ElectrometerProtocol::set_triggerdelay (std::string) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_triggerdelay( ).");
}

// ============================================================================
// ElectrometerProtocol::get_triggerdelay
// ============================================================================
std::string ElectrometerProtocol::get_triggerdelay (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_triggerdelay( ).");
}

// ============================================================================
// ElectrometerProtocol::set_triggerdelayAuto
// ============================================================================
void ElectrometerProtocol::set_triggerdelayAuto (std::string) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_triggerdelayAuto( ).");
}

// ============================================================================
// ElectrometerProtocol::get_triggerdelayAuto
// ============================================================================
std::string ElectrometerProtocol::get_triggerdelayAuto (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_triggerdelayAuto( ).");
}

// ============================================================================
// ElectrometerProtocol::set_averagecount
// ============================================================================
void ElectrometerProtocol::set_averagecount (std::string) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_averagecount( ).");
}

// ============================================================================
// ElectrometerProtocol::get_averagecount
// ============================================================================
std::string ElectrometerProtocol::get_averagecount (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_averagecount( ).");
}

// ============================================================================
// ElectrometerProtocol::set_averagecontrol
// ============================================================================
void ElectrometerProtocol::set_averagecontrol (std::string) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_averagecontrol( ).");
}

// ============================================================================
// ElectrometerProtocol::get_averagecontrol
// ============================================================================
std::string ElectrometerProtocol::get_averagecontrol (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_averagecontrol( ).");
}

// ============================================================================
// ElectrometerProtocol::clear_registers
// ============================================================================
void ElectrometerProtocol::clear_registers (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::clear_registers( ).");
}

// ============================================================================
// ElectrometerProtocol::averageStateON
// ============================================================================
void ElectrometerProtocol::averageStateON (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::averageStateON( ).");
}

// ============================================================================
// ElectrometerProtocol::averageStateOFF
// ============================================================================
void ElectrometerProtocol::averageStateOFF (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::averageStateOFF( ).");
}

// ============================================================================
// ElectrometerProtocol::set_buffer_size
// ============================================================================
void ElectrometerProtocol::set_buffer_size (std::string) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_buffer_size( ).");
}

// ============================================================================
// ElectrometerProtocol::get_buffer_size
// ============================================================================
std::string ElectrometerProtocol::get_buffer_size () 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_buffer_size( ).");
}

// ============================================================================
// ElectrometerProtocol::clear_buffer
// ============================================================================
void ElectrometerProtocol::clear_buffer (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::clear_buffer( ).");
}

// ============================================================================
// ElectrometerProtocol::store_raw_input
// ============================================================================
void ElectrometerProtocol::store_raw_input (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::store_raw_input( ).");
}

// ============================================================================
// ElectrometerProtocol::start_storing
// ============================================================================
void ElectrometerProtocol::start_storing (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::start_storing( ).");
}

// ============================================================================
// ElectrometerProtocol::enable_SRQBufferFull
// ============================================================================
void ElectrometerProtocol::enable_SRQBufferFull (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::enable_SRQBufferFull( ).");
}

// ============================================================================
// ElectrometerProtocol::disable_SRQBufferFull
// ============================================================================
void ElectrometerProtocol::disable_SRQBufferFull (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::disable_SRQBufferFull( ).");
}
  
// ============================================================================
// ElectrometerProtocol::SRQLineState
// ============================================================================
bool ElectrometerProtocol::SRQLineState (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::SRQLineState( ).");
}
  
// ============================================================================
// ElectrometerProtocol::readStatusByteRegister
// ============================================================================
short ElectrometerProtocol::readStatusByteRegister (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::readStatusByteRegister( ).");
}
  
// ============================================================================
// ElectrometerProtocol::get_overloadRangeState (DDC Devices !!!)
// ============================================================================
bool ElectrometerProtocol::get_overloadRangeState (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_overloadRangeState( ).");
}
  
// ============================================================================
// ElectrometerProtocol::set_conversionRate
// ============================================================================
void ElectrometerProtocol::set_conversionRate (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_conversionRate( ).");
}
  
// ============================================================================
// ElectrometerProtocol::enable_readingWithPrefix
// ============================================================================
void ElectrometerProtocol::enable_readingWithPrefix (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::enable_readingWithPrefix( ).");
}
  
// ============================================================================
// ElectrometerProtocol::disable_readingWithPrefix
// ============================================================================
void ElectrometerProtocol::disable_readingWithPrefix (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::disable_readingWithPrefix( ).");
}
  
// ============================================================================
// ElectrometerProtocol::enable_ReadingsFromElectrometer
// ============================================================================
void ElectrometerProtocol::enable_ReadingsFromElectrometer (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::enable_ReadingsFromElectrometer( ).");
}
  
// ============================================================================
// ElectrometerProtocol::enable_readingsFromBuffer_K617_6512
// ============================================================================
void ElectrometerProtocol::enable_readingsFromBuffer_K617_6512 (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::enable_readingsFromBuffer_K617_6512( ).");
}
  
// ============================================================================
// ElectrometerProtocol::enable_readingsFromBuffer_K486_487
// ============================================================================
void ElectrometerProtocol::enable_readingsFromBuffer_K486_487 (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::enable_readingsFromBuffer_K486_487( ).");
}
  
// ============================================================================
// ElectrometerProtocol::enable_integrationPeriod
// ============================================================================
void ElectrometerProtocol::enable_integrationPeriod (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::enable_integrationPeriod( ).");
}
  
// ============================================================================
// ElectrometerProtocol::read_data_with_no_timestamp
// ============================================================================
void ElectrometerProtocol::read_data_with_no_timestamp (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::read_data_with_no_timestamp( ).");
}

// ============================================================================
// ElectrometerProtocol::get_DDC_configuration
// ============================================================================
std::string ElectrometerProtocol::get_DDC_configuration (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_configuration( ).");
}

// ============================================================================
// ElectrometerProtocol::set_electrometer_active_channel
// ============================================================================
void ElectrometerProtocol::set_electrometer_active_channel (unsigned short ) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::set_electrometer_active_channel( ).");
}

// ============================================================================
// ElectrometerProtocol::get_electrometer_active_channel
// ============================================================================
unsigned short ElectrometerProtocol::get_electrometer_active_channel (void) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::get_electrometer_active_channel( ).");
}

// ============================================================================
// ElectrometerProtocol::save_configuration
// ============================================================================
void ElectrometerProtocol::save_configuration (unsigned short) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::save_configuration( ).");
}

// ============================================================================
// ElectrometerProtocol::restore_configuration
// ============================================================================
void ElectrometerProtocol::restore_configuration (unsigned short) 
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
											"This Electrometer does not support this command.",
											"ElectrometerProtocol::restore_configuration( ).");
}

