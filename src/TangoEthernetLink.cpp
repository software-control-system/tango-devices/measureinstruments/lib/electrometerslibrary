// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Electrometer Support Library
//
// = FILENAME
//    TangoEthernetLink.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>
#include <iostream>
#include "TangoEthernetLink.h"



// ============================================================================
// TangoEthernetLink::TangoEthernetLink
// ============================================================================
TangoEthernetLink::TangoEthernetLink (std::string& serial_device_name)
:	CommunicationLink(serial_device_name),
	_ethernet_proxy (0),
	_is_ethernet_proxy_ok (false)
{
}

// ============================================================================
// TangoEthernetLink::~TangoEthernetLink
// ============================================================================
TangoEthernetLink::~TangoEthernetLink (void)
{

	if(_ethernet_proxy)
	{
		delete _ethernet_proxy;
		_ethernet_proxy = 0;
		_is_ethernet_proxy_ok = false;
	}

}


// ============================================================================
// TangoEthernetLink::create_ethernet_proxy
// ============================================================================
void TangoEthernetLink::check_proxy (void)  throw (Tango::DevFailed)
{
  std::string description("");

	try
	{
		//- try
		this->_ethernet_proxy = new Tango::DeviceProxy(_communication_Device_name.c_str());
		_ethernet_proxy->ping();
		_is_ethernet_proxy_ok = true;
	}
	catch(Tango::DevFailed& df )
	{
		if( this->_ethernet_proxy )
		{
			delete this->_ethernet_proxy;
			this->_ethernet_proxy = 0;
		}
		_is_ethernet_proxy_ok = false;
		description = "Unable to create proxy on : " + _communication_Device_name;
		Tango::Except::re_throw_exception (df,
										(const char*)"ALLOCATION_ERROR",
										description.c_str(),
										(const char*)"TangoEthernetLink::create_ethernet_proxy");

	}
	catch(...)
	{
		if( this->_ethernet_proxy )
		{
			delete this->_ethernet_proxy;
			this->_ethernet_proxy = 0;
		}
		_is_ethernet_proxy_ok = false;
		description = "Unable to create proxy on : " + _communication_Device_name + "\nmemory allocation failed.";
		Tango::Except::throw_exception (
										(const char*)"ALLOCATION_ERROR",
										description.c_str(),
										(const char*)"TangoEthernetLink::write");
	}
}

// ============================================================================
// TangoEthernetLink::write
// ============================================================================
void TangoEthernetLink::write (std::string command_to_send)  throw (Tango::DevFailed)
{
  std::string description("");

	check_proxy();
	if (!_is_ethernet_proxy_ok)
		return;

	Tango::DeviceData dd_in;
	Tango::DevString argin = 0;

	try
	{
		argin = new char [command_to_send.size() + 1];
		strcpy(argin, command_to_send.c_str());
	}
	catch(Tango::DevFailed& df )
	{
		if( argin )
		{
			delete [] argin;
			argin = 0;
		}
		description = "Unable to write command : " + command_to_send +  + "\nmemory allocation failed.";
		Tango::Except::re_throw_exception (df,
										(const char*)"ALLOCATION_ERROR",
										description.c_str(),
										(const char*)"TangoEthernetLink::write");
	}
	catch(...)
	{
		description = "Unable to write command : " + command_to_send + "\nmemory allocation failed.";
		Tango::Except::throw_exception (
										(const char*)"ALLOCATION_ERROR",
										description.c_str(),
										(const char*)"TangoEthernetLink::write");
	}

	try
	{
		//- try
		dd_in << argin;
		_ethernet_proxy->command_inout("Write", dd_in);
		if( argin )
		{
			delete [] argin;
			argin = 0;
		}
		//std::cout << "TangoEthernetLink::write -> argin = *\"" << argin << "\"*" << std::endl;
	}
	catch(Tango::DevFailed& df )
	{
		if( argin )
		{
			delete [] argin;
			argin = 0;
		}
		description = "Unable to write command : " + command_to_send;
		Tango::Except::re_throw_exception (df,
										(const char*)"COMMUNICATION_ERROR",
										description.c_str(),
										(const char*)"TangoEthernetLink::write");

	}
	catch(...)
	{
		if( argin )
		{
			delete [] argin;
			argin = 0;
		}
		description = "Unable to write command : " + command_to_send;
		Tango::Except::throw_exception (
										(const char*)"COMMUNICATION_ERROR",
										description.c_str(),
										(const char*)"TangoEthernetLink::write");
	}
}


// ============================================================================
// TangoEthernetLink::read
// ============================================================================
std::string TangoEthernetLink::read (void) throw (Tango::DevFailed)
{

	check_proxy();
	if(!_is_ethernet_proxy_ok)
		return "";

	Tango::DeviceData dd_out;
	std::string response("");

	try
	{

		dd_out = _ethernet_proxy->command_inout("Read");
		dd_out >> response;
	}
	catch(Tango::DevFailed& df )
	{
		Tango::Except::re_throw_exception (df,
										(const char*)"COMMUNICATION_ERROR",
										(const char*)"Unable to perform a read operation",
										(const char*)"TangoEthernetLink::read");
	}
	catch(...)
	{
		Tango::Except::throw_exception (
										(const char*)"COMMUNICATION_ERROR",
										(const char*)"Unable to perform a read operation",
										(const char*)"TangoEthernetLink::read");
	}

	return response ;
}

// ============================================================================
// TangoEthernetLink::write_read : read N char
// ============================================================================
std::string TangoEthernetLink::write_read (std::string command_to_send, size_t nbChar) throw (Tango::DevFailed)
{
	std::string resp;

	check_proxy();
	if(!_is_ethernet_proxy_ok)
		return "";

	Tango::DeviceData dd_in;
	Tango::DevString argin = 0;

	Tango::DeviceData dd_out;
	std::string response("");

	try
	{
		argin = new char [command_to_send.size() + 1];
		strcpy(argin, command_to_send.c_str());
		dd_in << argin;
		dd_out = _ethernet_proxy->command_inout("WriteRead", dd_in);
		dd_out >> response;
	}
	catch(Tango::DevFailed& df )
	{
		Tango::Except::re_throw_exception (df,
										(const char*)"COMMUNICATION_ERROR",
										(const char*)"Unable to perform a read operation",
										(const char*)"TangoEthernetLink::read");
	}
	catch(...)
	{
		Tango::Except::throw_exception (
										(const char*)"COMMUNICATION_ERROR",
										(const char*)"Unable to perform a read operation",
										(const char*)"TangoEthernetLink::read");
	}

	return response ;


}

