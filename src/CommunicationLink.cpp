// ============================================================================
//
// = CONTEXT
//    TANGO Project - Electrometer Support Library
//
// = FILENAME
//    CommunicationLink.cpp
//
// = AUTHOR
//    X. Elattaoui
//
//
// $Author: xavela $
//
// $Revision: 1.6 $
//
// $Log: not supported by cvs2svn $
// Revision 1.5  2008/04/15 12:52:14  xavela
// xavier :
// - SRQ management changed :
// * ReadStatusByteRegister method added
// * IsSrqLineUP added
//
// Revision 1.4  2008/02/15 10:17:57  xavela
// xavier :
// - command abort added for SCPI Keithleys
//
// Revision 1.3  2008/02/13 15:51:44  xavela
// xavier :
// - Integration Mode available for DDC/SCPI devices
// - tests done with a K617 (DDC) and a K6485 (SCPI)
//
// Revision 1.2  2007/11/27 15:23:02  xavela
// xavier :
// - modif done on Novelec part :
// bug fix
//
// Revision 1.1  2007/07/09 13:20:37  stephle
// initial import
//
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include "CommunicationLink.h"
#include "ElectrometerException.h"

// ============================================================================
// CommunicationLink::CommunicationLink
// ============================================================================
//CommunicationLink::CommunicationLink (const std::string& communication_link_name)
CommunicationLink::CommunicationLink ( std::string& communication_link_name)
:	_communication_Device_name(communication_link_name)
{
	//std::cout << "CommunicationLink::CommunicationLink <-" << std::endl;


	//std::cout << "CommunicationLink::CommunicationLink ->" << std::endl;
}

// ============================================================================
// CommunicationLink::~CommunicationLink
// ============================================================================
CommunicationLink::~CommunicationLink (void)
{
	//std::cout << "CommunicationLink::~CommunicationLink <-" << std::endl;


	//std::cout << "CommunicationLink::~CommunicationLink ->" << std::endl;
}

// ============================================================================
// CommunicationLink::SRQLineState
// ============================================================================
bool CommunicationLink::SRQLineState (void)
{
		throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
												"This protocol cannot check the SRQ line state.",
												"CommunicationLink::SRQLineState( ).");
}

// ============================================================================
// CommunicationLink::readStatusByteRegister
// ============================================================================
short CommunicationLink::readStatusByteRegister (void)
{
		throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
												"This protocol cannot check the device staus byte register.",
												"CommunicationLink::readStatusByteRegister( ).");
}

// ============================================================================
// CommunicationLink::clear
// ============================================================================
void CommunicationLink::clear (void)
{
		throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
												"This protocol cannot send interface clear command.",
												"CommunicationLink::clear( ).");
}

// ============================================================================
// CommunicationLink::trigger
// ============================================================================
void CommunicationLink::trigger (void)
{
		throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED", 
												"This protocol cannot send a trigger command.",
												"CommunicationLink::trigger( ).");
}

