// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Electrometer Support Library
//
// = FILENAME
//    TangoGpibLink.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>
#include <iostream>
#include "TangoGpibLink.h"


// ============================================================================
// TangoGpibLink::TangoGpibLink
// ============================================================================
TangoGpibLink::TangoGpibLink (std::string& gpib_device_name)
:	CommunicationLink(gpib_device_name),
  _gpib_proxy (0)
{
	std::cout << "TangoGpibLink::TangoGpibLink <-" << std::endl;

	std::cout << "TangoGpibLink::TangoGpibLink ->" << std::endl;
}

// ============================================================================
// TangoGpibLink::~TangoGpibLink
// ============================================================================
TangoGpibLink::~TangoGpibLink (void)
{
	std::cout << "TangoGpibLink::~TangoGpibLink <-" << std::endl;

	if(_gpib_proxy)
	{
		delete _gpib_proxy;
		_gpib_proxy = 0;
	}

	std::cout << "TangoGpibLink::~TangoGpibLink ->" << std::endl;
}


// ============================================================================
// TangoGpibLink::create_gpib_proxy
// ============================================================================
void TangoGpibLink::create_gpib_proxy (void)  throw (Tango::DevFailed)
{
  std::string description("");

	try
	{
		//- try
		this->_gpib_proxy = new Tango::DeviceProxy(_communication_Device_name.c_str());
	}
	catch(Tango::DevFailed& df )
	{
		description = "Unable to create proxy on : " + _communication_Device_name + ". Check GPIB device is up !";

		Tango::Except::re_throw_exception (df,
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::create_gpib_proxy");
	}
	catch(...)
	{
		description = "Unable to create proxy on : " + _communication_Device_name + ". Check GPIB device is up !";
		Tango::Except::throw_exception (
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::create_gpib_proxy");
	}
}


// ============================================================================
// TangoGpibLink::write
// ============================================================================
void TangoGpibLink::write (std::string command_to_send)  throw (Tango::DevFailed)
{
  Tango::DeviceData dd_in;
  Tango::DevString cmd = 0;
  std::string description("");

	if(!_gpib_proxy)
		create_gpib_proxy();
	try
	{
		cmd = new char [command_to_send.size() + 1];
		strcpy(cmd, command_to_send.c_str());
	}
	catch(Tango::DevFailed& df )
	{
		if ( cmd )
			delete [] cmd;
		description = "Failed to allocate memory for the command to write";
		Tango::Except::re_throw_exception (df,
			(const char*)"MEMORY_ALLOCATION",
			description.c_str(),
			(const char*)"TangoGpibLink::write");
	}
	catch(...)
	{
		if ( cmd )
			delete [] cmd;
		Tango::Except::throw_exception (
			(const char*)"MEMORY_ALLOCATION",
			(const char*)"Not enough memory.",
			(const char*)"TangoGpibLink::write");
	}

	try
	{
		//- try
		dd_in << cmd;
		this->_gpib_proxy->command_inout("Write",  dd_in);
		if ( cmd )
			delete [] cmd;
	}
	catch(Tango::DevFailed& df )
	{
		if ( cmd )
			delete [] cmd;
		description = "Unable to write command : " + command_to_send + ". Caught DevFailed." ;
		Tango::Except::re_throw_exception (df,
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::write");

	}
	catch(...)
	{
		if ( cmd )
			delete [] cmd;
		description = "Unable to write command : " + command_to_send + ". Caught [...]." ;
		Tango::Except::throw_exception (
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::write");
	}
}


// ============================================================================
// TangoGpibLink::read
// ============================================================================
std::string TangoGpibLink::read (void) throw (Tango::DevFailed)
{
    Tango::DeviceData dd_out;
    std::string response("");

	if(!_gpib_proxy)
		create_gpib_proxy();

	try
	{
		//- try
		dd_out = this->_gpib_proxy->command_inout("Read");
		dd_out >> response;
	}
	catch(Tango::DevFailed& df )
	{
		Tango::Except::re_throw_exception (df,
			(const char*)"COMMUNICATION_ERROR",
			(const char*)"Unable to perform a read operation",
			(const char*)"TangoGpibLink::read");
	}
	catch(...)
	{
		Tango::Except::throw_exception (
			(const char*)"COMMUNICATION_ERROR",
			(const char*)"Unable to perform a read operation, caught [...].",
			(const char*)"TangoGpibLink::read");
	}

	return response;
}

// ============================================================================
// TangoGpibLink::write_read
// ============================================================================
std::string TangoGpibLink::write_read (std::string command_to_send, size_t /*unusedLgth*/) throw (Tango::DevFailed)
{
  std::string description("");
  std::string response("");

	if(!_gpib_proxy)
		create_gpib_proxy();
	Tango::DeviceData dd_in, dd_out;
	Tango::DevString argin = 0;

	try
	{
		//- try
		argin = new char [command_to_send.size() + 1];
		strcpy(argin, command_to_send.c_str());
		dd_in << argin;
		dd_out = this->_gpib_proxy->command_inout("WriteRead",  dd_in);
		dd_out >> response;
		if ( argin )
			delete [] argin;
	}
	catch(Tango::DevFailed& df )
	{
		if ( argin )
			delete [] argin;
		description = "Unable to write/read command : " + command_to_send + " and read device response.";
		Tango::Except::re_throw_exception (df,
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::write_read");
	}
	catch(...)
	{
		if ( argin )
			delete [] argin;
		description = "Unable to write/read command : " + command_to_send + " and read device response.";
		Tango::Except::throw_exception (
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::write_read");
	}

	return response;
}

// ============================================================================
// TangoGpibLink::SRQLineState
// ============================================================================
bool TangoGpibLink::SRQLineState (void) throw (Tango::DevFailed)
{
  std::string description("");
  bool result = false;
  Tango::DeviceData dd_out;

	if(!_gpib_proxy)
		create_gpib_proxy();

	try
	{
		//- try
		dd_out = this->_gpib_proxy->command_inout("IsSRQLineUP");
		dd_out >> result;
	}
	catch(Tango::DevFailed& df )
	{
		description = "Unable to get Gpib SRQ line state.";

		Tango::Except::re_throw_exception (df,
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::SRQLineState");

	}
	catch(...)
	{
		description = "Unable to get Gpib SRQ line state.";
		Tango::Except::throw_exception (
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::SRQLineState");
	}

	return result;
}

// ============================================================================
// TangoGpibLink::readStatusByteRegister
// ============================================================================
short TangoGpibLink::readStatusByteRegister (void) throw (Tango::DevFailed)
{
  std::string description("");
  short result = -1;
  Tango::DeviceAttribute da_out;


	if(!_gpib_proxy)
		create_gpib_proxy();

	try
	{
		//- try
		da_out = this->_gpib_proxy->read_attribute("statusByteRegister");
		da_out >> result;
	}
	catch(Tango::DevFailed& df )
	{
		description = "Unable to get device status byte register.";

		Tango::Except::re_throw_exception (df,
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::readStatusByteRegister");

	}
	catch(...)
	{
		description = "Unable to get device status byte register.";
		Tango::Except::throw_exception (
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::readStatusByteRegister");
	}

	return result;
}

// ============================================================================
// TangoGpibLink::clear
// ============================================================================
void TangoGpibLink::clear (void) throw (Tango::DevFailed)
{
  std::string description("");

	if(!_gpib_proxy)
		create_gpib_proxy();

	try
	{
		//- try
		this->_gpib_proxy->command_inout("Clear");
	}
	catch(Tango::DevFailed& df )
	{
		description = "Unable send Clear Device command.";
		Tango::Except::re_throw_exception (df,
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::clear");
	}
	catch(...)
	{
		description = "Unable send Clear Device command.";
		Tango::Except::throw_exception (
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::clear");
	}
}

// ============================================================================
// TangoGpibLink::trigger
// ============================================================================
void TangoGpibLink::trigger (void) throw (Tango::DevFailed)
{
  std::string description("");

	if(!_gpib_proxy)
		create_gpib_proxy();

	try
	{
		//- try
		this->_gpib_proxy->command_inout("Trigger");
	}
	catch(Tango::DevFailed& df )
	{
		description = "Unable send Trigger command.";
		Tango::Except::re_throw_exception (df,
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::trigger");
	}
	catch(...)
	{
		description = "Unable send Trigger command.";
		Tango::Except::throw_exception (
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"TangoGpibLink::trigger");
	}
}

