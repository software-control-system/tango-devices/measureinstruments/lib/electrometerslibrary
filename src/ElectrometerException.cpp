//******************************************************************************************
//
//
//		july 24, 2006 :  Source file for the Electrometers exceptions
//					     (available for all models Keithley and Novelec)
//
//
//		author : X.Elattaoui
//
//		ElectrometerException.cpp: interface for the communication exceptions class.
//
//
// $Author: xavela $
//
// $Revision: 1.3 $
//
// $Log: not supported by cvs2svn $
// Revision 1.2  2008/02/15 10:17:57  xavela
// xavier :
// - command abort added for SCPI Keithleys
//
// Revision 1.1  2007/07/09 13:20:37  stephle
// initial import
//
//
//******************************************************************************************

// ============================================================================
// DEPENDENCIES
#include "ElectrometerException.h"

namespace electrometer {

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (void)
  :  reason ("unknown"),
     desc ("unknown error"),
     origin ("unknown"),
     severity (electrometer::ERR)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const char *_reason,
	      const char *_desc,
	      const char *_origin,
	      int _severity)
  :  reason (_reason),
     desc (_desc),
     origin (_origin),
     severity (_severity)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const std::string& _reason,
	      const std::string& _desc,
	      const std::string& _origin,
	      int _severity)
  :  reason (_reason),
     desc (_desc),
     origin (_origin),
     severity (_severity)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const Error& _src)
  :  reason (_src.reason),
     desc (_src.desc),
     origin (_src.origin),
     severity (_src.severity)
{

}

// ============================================================================
// Error::~Error
// ============================================================================
Error::~Error (void)
{

}

// ============================================================================
// Error::operator=
// ============================================================================
Error& Error::operator= (const Error& _src) 
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }

  this->reason	= _src.reason;
  this->desc	= _src.desc;
  this->origin	= _src.origin;
  this->severity = _src.severity;

  return *this;
}

// ============================================================================
// ElectrometerException::ElectrometerException
// ============================================================================
ElectrometerException::ElectrometerException (void)
  : errors(0)
{
  this->push_error(Error());
}

// ============================================================================
// ElectrometerException::ElectrometerException
// ============================================================================
ElectrometerException::ElectrometerException (const char *_reason,
		      const char *_desc,
		      const char *_origin,
		      int _severity) 
  : errors(0)
{

  this->push_error(Error(_reason, _desc, _origin, _severity));
}

// ============================================================================
// ElectrometerException::ElectrometerException
// ============================================================================
ElectrometerException::ElectrometerException (const std::string& _reason,
		      const std::string& _desc,
		      const std::string& _origin,
		      int _severity) 
  : errors(0)
{
  this->push_error(_reason, _desc, _origin,_severity);
}

// ============================================================================
// ElectrometerException::ElectrometerException
// ============================================================================
ElectrometerException::ElectrometerException (const ElectrometerException& _src)
  : errors(0)
{
  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }
}

// ============================================================================
// ElectrometerException::ElectrometerException
// ============================================================================
ElectrometerException& ElectrometerException::operator= (const ElectrometerException& _src)
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }

  this->errors.clear();

  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }

  return *this;
}

// ============================================================================
// ElectrometerException::~ElectrometerException
// ============================================================================
ElectrometerException::~ElectrometerException (void)
{
  this->errors.clear();
}


// ============================================================================
// ElectrometerException::push_error
// ============================================================================
void ElectrometerException::push_error (const char *_reason,
						      const char *_desc,
							  const char *_origin, 
			    int _severity)
{
  this->errors.push_back(Error(_reason, _desc, _origin, _severity));
}

// ============================================================================
// ElectrometerException::push_error
// ============================================================================
void ElectrometerException::push_error (const std::string& _reason,
						      const std::string& _desc,
							  const std::string& _origin, 
			    int _severity)
{
  this->errors.push_back(Error(_reason, _desc, _origin, _severity));
}

// ============================================================================
// ElectrometerException::push_error
// ============================================================================
void ElectrometerException::push_error (const Error& _error)
{
  this->errors.push_back(_error);
}


} // namespace electrometer


