// ============================================================================
//
// = CONTEXT
//    TANGO Project - NovelecElectrometer Support Library
//			PhotoConducteur Types are Type 4 & 5 only
//
// = FILENAME
//    N_PhotoConducteur.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <sstream>
#include <string>
#include "N_PhotoConducteur.h"
#include "NovelecProtocol.h"
/*
* Valid Range values for a N_PhotoConducteur
*/
static const std::vector<std::string> NType4_rangeValue {"100","30","10","3"};			//- MOhms
static const std::vector<std::string> NType5_rangeValue {"1000","300","100","30"};	//- KOhms


// ============================================================================
// N_PhotoConducteur::N_PhotoConducteur
// ============================================================================
N_PhotoConducteur::N_PhotoConducteur (std::string& comLink_device_name, short address, std::string comProtocol)
:	Novelec_MCCE2(comLink_device_name, address, comProtocol)
{
	//std::cout << "N_PhotoConducteur::N_PhotoConducteur <-" << std::endl;

	_rangeLimit = 3;

	//std::cout << "N_PhotoConducteur::N_PhotoConducteur ->" << std::endl;
}

// ============================================================================
// N_PhotoConducteur::~N_PhotoConducteur
// ============================================================================
N_PhotoConducteur::~N_PhotoConducteur (void)
{
	std::cout << "N_PhotoConducteur::~N_PhotoConducteur <-" << std::endl;

	//std::cout << "N_PhotoConducteur::~N_PhotoConducteur ->" << std::endl;
}

// ============================================================================
// N_PhotoConducteur::range_up
// ============================================================================
void N_PhotoConducteur::range_up (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoConducteur::range_up( ).");

std::stringstream cmd_to_send;

	_range += 1;

	if(_range > _rangeLimit)
	{
		_range = _rangeLimit;
		throw electrometer::ElectrometerException("OUT_OF_RANGE",
												"Range up limit reached.",
												"N_PhotoConducteur::range_up( ).");
	}

	//- build and send the command
	cmd_to_send << _range;

	_electrometerProtocol->set_range(cmd_to_send.str());
}

// ============================================================================
// N_PhotoConducteur::range_down
// ============================================================================
void N_PhotoConducteur::range_down (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoConducteur::range_down( ).");

std::stringstream cmd_to_send;

	_range -= 1;

	if(_range < 0)
	{
		_range = 0;
		throw electrometer::ElectrometerException("OUT_OF_RANGE",
												"Range down limit reached.",
												"N_PhotoConducteur::range_down( ).");
	}

	//- build and send the command
//	_rangeStr = NType1_rangeValue[_range];
	cmd_to_send << _range;

	_electrometerProtocol->set_range(cmd_to_send.str());
}

// ============================================================================
// N_PhotoConducteur::get_ElectroMeterGain
// ============================================================================
std::string N_PhotoConducteur::get_ElectroMeterGain (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoConducteur::get_ElectroMeterGain( ).");

  return _electrometerProtocol->get_gain();
}

// ============================================================================
// N_PhotoConducteur::set_ElectroMeterGain
// ============================================================================
void N_PhotoConducteur::set_ElectroMeterGain (std::string gain)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoConducteur::set_ElectroMeterGain( ).");

	_electrometerProtocol->set_gain(gain);
}

// ============================================================================
// N_PhotoConducteur::set_ElectroMeterRange
// ============================================================================
void N_PhotoConducteur::set_ElectroMeterRange (std::string rgStr)
{
std::stringstream range_cmd_to_send;

  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoConducteur::set_ElectroMeterRange( ).");

  //- switch the novelec type :
	switch(_MCCE2electroTypeNumber)
	{
	case 4 : _range = Novelec_MCCE2::check_range_value(rgStr, NType4_rangeValue);
		break;
	case 5 : _range = Novelec_MCCE2::check_range_value(rgStr, NType5_rangeValue);
		break;
	}

	if (_range < 0)
		throw electrometer::ElectrometerException("INVALID_PARAMETER",
												  "This electrometer does not support this range value.",
												  "N_PhotoConducteur::set_ElectroMeterRange( ).");
	//- it is OK
	range_cmd_to_send << _range;

	_electrometerProtocol->set_range(range_cmd_to_send.str());
}

// ============================================================================
// N_PhotoConducteur::get_ElectroMeterRange
// ============================================================================
std::string N_PhotoConducteur::get_ElectroMeterRange()
{
std::string rangeStr("");

  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"N_PhotoConducteur::get_ElectroMeterRange( ).");

	rangeStr = this->_electrometerProtocol->get_range();

	//- switch the novelec type :
	switch(_MCCE2electroTypeNumber)
	{
	case 4 : _range = Novelec_MCCE2::check_range_value(rangeStr, NType4_rangeValue);
		break;
	case 5 : _range = Novelec_MCCE2::check_range_value(rangeStr, NType5_rangeValue);
		break;
	}

	if (_range < 0)
		throw electrometer::ElectrometerException("INVALID_PARAMETER",
												  "Cannot find the applied range value.",
												  "N_PhotoConducteur::get_ElectroMeterRange( ).");

	return rangeStr;
}

// ============================================================================
// N_PhotoConducteur::ranges_list
// ============================================================================
std::vector<std::string> N_PhotoConducteur::ranges_list(void)
{
  std::vector<std::string> vrangeslist;

	//- switch the novelec type :
	switch(_MCCE2electroTypeNumber)
	{
	case 4 : vrangeslist = NType4_rangeValue;
		break;
	case 5 : vrangeslist = NType5_rangeValue;
		break;
	}
	return vrangeslist;
}
