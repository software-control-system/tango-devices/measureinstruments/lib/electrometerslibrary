// ============================================================================
//
// = CONTEXT
//    TANGO Project - NovelecElectrometer Support Library
//
// = FILENAME
//    Novelec_MCCE2.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <sstream>
#include <string>
#include <yat/utils/XString.h>
#include "NovelecProtocol.h"
#include "Novelec_MCCE2.h"


// ============================================================================
// Novelec_MCCE2::Novelec_MCCE2
// ============================================================================
/*Novelec_MCCE2::Novelec_MCCE2 (std::string& comLink_device_name, unsigned short address, unsigned short novTypeNumber)
:	AbstractElectrometerClass(comLink_device_name),
	_rangeLimit(0),
  _address(address),
	_MCCE2electroTypeNumber(novTypeNumber)
{
	//std::cout << "Novelec_MCCE2::Novelec_MCCE2 <-" << std::endl;

	//std::cout << "Novelec_MCCE2::Novelec_MCCE2 ->" << std::endl;
}
*/
// ============================================================================
// Novelec_MCCE2::Novelec_MCCE2
// ============================================================================
Novelec_MCCE2::Novelec_MCCE2 (std::string& comLink_device_name, unsigned short address, std::string comProtocol)
:	AbstractElectrometerClass(comLink_device_name),
	_rangeLimit(0),
  _address(address),
	_MCCE2electroTypeNumber(0),
	_MCCE2electroTypeStr("Unknown type"),
	_comProtocol(comProtocol)
{
	std::cout << "\t\tNovelec_MCCE2::Novelec_MCCE2 <-" <<comProtocol<< std::endl;


}

// ============================================================================
// Novelec_MCCE2::~Novelec_MCCE2
// ============================================================================
Novelec_MCCE2::~Novelec_MCCE2 (void)
{
	//std::cout << "Novelec_MCCE2::~Novelec_MCCE2 <-" << std::endl;

}

// ============================================================================
// Novelec_MCCE2::init_protocol
// ============================================================================
bool Novelec_MCCE2::init_protocol (void)
{
  std::string description("");
  bool success = false;
  try
  {
	  //- build the keithley Electrometer protocol obj
	  _electrometerProtocol = new NovelecProtocol(_device_proxy_name, _address,_comProtocol);

    if(_electrometerProtocol)
      success = _electrometerProtocol->build_communicationLink();

    //- find electrometer type :
    std::string elecTypeStr("");
    NovelecProtocol* _pNP = dynamic_cast<NovelecProtocol*>(_electrometerProtocol);
    if(_pNP)
      elecTypeStr = _pNP->check_electrotype();

    //- get the connected electrometer type :
    if(_pNP)
      this->_MCCE2electroTypeNumber = _pNP->get_electrometerType();

    switch(_MCCE2electroTypeNumber)
    {
    case 0 : this->_MCCE2electroTypeStr = "Not connected";
      success = false;
        break;
    case 1 : this->_MCCE2electroTypeStr = "UHS photovoltaic";
      _rangeLimit = 3;
      break;
    case 2 : this->_MCCE2electroTypeStr = "HS photovoltaic";
      _rangeLimit = 7;
        break;
    case 3 : this->_MCCE2electroTypeStr = "MS photovoltaic";
      _rangeLimit = 7;
      break;
	case 4 :	this->_MCCE2electroTypeStr = "Photoconducteur HS";
      _rangeLimit = 3;
		break;
	case 5 :	this->_MCCE2electroTypeStr = "Photoconducteur MS";
      _rangeLimit = 3;
		break;
	default :	set_electroState(FAULT);
                success = false;
    }
  }
  catch(Tango::DevFailed& df)
  {
		description = "FAILED to create proxy on : " + _device_proxy_name;

		Tango::Except::re_throw_exception (df,
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"Novelec_MCCE2::init_protocol");
  }
  catch(...)
  {
		description = "FAILED to create proxy on : " + _device_proxy_name + ". Caught [...].";

		Tango::Except::throw_exception (
			(const char*)"COMMUNICATION_ERROR",
			description.c_str(),
			(const char*)"Novelec_MCCE2::init_protocol");
  }
  return success;
}

// ============================================================================
// Novelec_MCCE2::get_ElectroMeterPolarity
// ============================================================================
std::string Novelec_MCCE2::get_ElectroMeterPolarity (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::get_ElectroMeterPolarity( ).");

	return _electrometerProtocol->get_polarity();
}

// ============================================================================
// Novelec_MCCE2::set_ElectroMeterPolarity
// ============================================================================
void Novelec_MCCE2::set_ElectroMeterPolarity (std::string polarity)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::set_ElectroMeterPolarity( ).");

	_electrometerProtocol->set_polarity(polarity);
}

// ============================================================================
// Novelec_MCCE2::set_Zero_VonF_function
// ============================================================================
void Novelec_MCCE2::set_Zero_VonF_function (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::set_Zero_VonF_function( ).");

	_electrometerProtocol->unable_zeroVF_func();
}

// ============================================================================
// Novelec_MCCE2::set_Offset_function
// ============================================================================
void Novelec_MCCE2::set_Offset_ZeroV1_function (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::set_Offset_ZeroV1_function( ).");

	_electrometerProtocol->unable_offset_zeroV1_func();
}

// ============================================================================
// Novelec_MCCE2::set_Leakage_ZeroV2_function
// ============================================================================
void Novelec_MCCE2::set_Leakage_ZeroV2_function (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::set_Leakage_ZeroV2_function( ).");

	_electrometerProtocol->unable_leakage_zeroV2_func();
}

// ============================================================================
// Novelec_MCCE2::set_Test_function
// ============================================================================
void Novelec_MCCE2::set_Test_function (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::set_Test_function( ).");

	_electrometerProtocol->unable_test_func();
}

// ============================================================================
// Novelec_MCCE2::set_Measure_function
// ============================================================================
void Novelec_MCCE2::set_Measure_function (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::set_Measure_function( ).");

	_electrometerProtocol->unable_measure_func();
}

// ============================================================================
// Novelec_MCCE2::mcce_on
// ============================================================================
void Novelec_MCCE2::mcce_on (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::mcce_on( ).");

	_electrometerProtocol->switch_MCCE2_ON();
}

// ============================================================================
// Novelec_MCCE2::mcce_off
// ============================================================================
void Novelec_MCCE2::mcce_off (void)
{
  if( !this->_electrometerProtocol )
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::mcce_off( ).");

	_electrometerProtocol->switch_MCCE2_OFF();
}

// ============================================================================
// Novelec_MCCE2::electrometer_status
// ============================================================================
std::string Novelec_MCCE2::electrometer_status (void)
{
	std::string argout("");
	std::string tmp(" ");
	short status	= 0;

  if( !this->_electrometerProtocol )
  {
		throw electrometer::ElectrometerException("INIT_ERROR",
												"Novelec protocol initialization failed.",
												"Novelec_MCCE2::electrometer_status( ).");
	}

	NovelecProtocol* nproto = dynamic_cast<NovelecProtocol*>(_electrometerProtocol);
	if( !nproto )
	{
		throw electrometer::ElectrometerException("BAD_ALLOC",
												"Memory allocation failed to query the electrmometer status.",
												"Novelec_MCCE2::electrometer_status( ).");
	}

  /***********************************************************************
          DISABLED PART TO INCREASE SERIAL PERFORMANCE
  ***********************************************************************/

	argout = "\n\nINFOS :\n";
	argout += "----------\n";

	argout += "Novelec type : " + this->_MCCE2electroTypeStr + "\n";

	//- get Electrometer mode :
	argout += "Novelec mode : " + _electrometerProtocol->get_mode() + "\n";

  /***********************************************************************/

	try
	{
		//- read novelec status from HW
		tmp		= _electrometerProtocol->get_raw_status();

		status	= yat::XString<short>::to_num(tmp);
	}
	catch(...)
	{
		set_electroState(FAULT);

		throw electrometer::ElectrometerException("COMMUNICATION_ERROR",
												"Unable to query the electrmometer status.",
												"Novelec_MCCE2::electrometer_status( ).");
	}

	argout += "\nStatus detail :\n";
	argout += "------------------------\n";

	//- build novelec status
	if(status & 0x10)
		argout += "Measuring\n";
	else
		argout += "Not measuring\n";
	if(status & 0x20)
		argout += "Default parameters\n";
	else
		argout += "Ram OK\n";
	if(status & 0x40)
		argout += "Invalid parameter\n";
	else
		argout += "Parameter OK\n";
	if(status & 0x80)
	{
		argout += "ERREUR\n";
		set_electroState(ALARM);
	}
	else
	{
		argout += "No error\n";
		set_electroState(ON);
	}

	//- error(s) detail(s)
	if(status & 0x80)
	{
	  short errors = -1;
		try
		{
			//- read novelec error(s) from HW
			tmp		= nproto->get_errors();
			errors= yat::XString<short>::to_num(tmp);
		}
		catch(...)
		{
			set_electroState(FAULT);
			throw electrometer::ElectrometerException("INTERNAL_ERROR",
													"Unable to query the electrmometer errors.",
													"Novelec_MCCE2::electrometer_status( ).");
		}

		argout += "\nError(s) detail(s) :\n";
		argout += "------------------------\n";
    bool change_state_to_alarm = true;

		if(errors & 0x01)
			argout += "Default parameter\n";
		if(errors & 0x02)
			argout += "Serial error\n";
		if(errors & 0x04)
			argout += "Invalid parameter\n";
		if(errors & 0x08)
    {
			argout += "Overloaded electrometer\n";
      change_state_to_alarm = false;
    }
		if(errors & 0x10)
    {
			argout += "Eletrometer blocked (Polarity error ?)\n";
      change_state_to_alarm = false;
    }
		if(errors & 0x20)
			argout += "HT < low limit\n";
		if(errors & 0x40)
			argout += "HT > high limit\n";
		if(errors & 0x80)
			argout += "Initialization\n";
		if(errors & 0x100)
			argout += "Battery default\n";
		if(errors & 0x200)
			argout += "Power supply default\n";
		if(errors & 0x400)
			argout += "Unknown electrometer type\n";
		if(errors & 0x800)
			argout += "Disconnected electrometer\n";

    //- set state according to specifications : if "bloqu�" or "satur�" do not change the state to ALARM !!
    if ( change_state_to_alarm )
      set_electroState(ALARM);
	}

	return argout;
}

// ============================================================================
// Novelec_MCCE2::check_range_value
// ============================================================================
short Novelec_MCCE2::check_range_value (const std::string& rgToCheck, const std::vector<std::string> electroRangeList)
{
	std::string electroRg("");
	for (unsigned int idx = 0; idx <= _rangeLimit; idx++)
	{
		electroRg = electroRangeList[idx];
/* DEBUG		std::cout << "\t Novelec_MCCE2::check_range_value idx = " << idx
			<< " rgToCheck = $" << rgToCheck << "$"
			<< " electroRg = $" << electroRg << "$"
			<< std::endl;*/
		if( electroRg.find(rgToCheck) != std::string::npos )
			{
				//- value is in the list
				return idx;
			}
	}

	//- new range value not found in the electrometer range list !
	return -1;
}
