// ============================================================================
//
// = CONTEXT
//    TANGO Project - Abstract Electrometer Class Support Library
//
// = FILENAME
//    AbstractElectrometerClass.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// $Author: xavela $
//
// $Revision: 1.26 $
//
// $Log: not supported by cvs2svn $
// Revision 1.25  2012/07/11 08:21:54  xavela
// for test purpose only !
//
// Revision 1.24  2012/07/03 06:15:53  xavela
// Novelec part : bug at init fixed.
//
// Revision 1.23  2011/03/14 14:42:22  xavela
// SaveRestoreConfiguration command added. (Available only for SCPI Keithley devices)
//
// Revision 1.22  2010/06/10 14:36:15  xavela
// cmd switch_off called before any parameters modifications and then locked with switch_on.
//
// Revision 1.21  2010/03/17 11:57:03  xavela
// VSource control added !
//
// Revision 1.20  2010/03/17 10:00:15  xavela
// VSource output added for K_6517
//
// Revision 1.19  2009/10/14 15:26:17  xavela
// KeithleyMemory leak fixed.
// Novelec part updated
//
// Revision 1.18  2009/10/12 08:50:41  xavela
// minor change : cmd reset (*RST) added for DDCs.
//
// Revision 1.17  2009/03/18 10:25:29  xavela
// xavier :
// - Novelec part updated and tested !
// - +no more compilation warnings
//
// Revision 1.16  2009/03/10 10:28:52  xavela
// xavier:
// range values ordered
// set_range added
//
// Revision 1.15  2009/01/19 12:00:25  xavela
// xavier :
// - MCCE2 (Novelec part) : methods to change the active channel to communicate with.
//
// Revision 1.14  2008/06/20 14:36:23  xavela
// xavier :
// - DDC status show only errors if any
// - added command to get DDC model configuration
//
// Revision 1.13  2008/05/14 09:42:37  xavela
// xavier :
// - attributes management :
// -> integrationTime, buffersize and triggerMode now well initialised
//
// TODO : after a Start command
// -> declare all attributes INVALID !?
//
// Revision 1.12  2008/04/30 15:57:31  xavela
// xavier :
// 6517 model management added and tested :
// -> problem to check : no SRQ !
//
// Revision 1.11  2008/04/15 12:52:14  xavela
// xavier :
// - SRQ management changed :
// * ReadStatusByteRegister method added
// * IsSrqLineUP added
//
// Revision 1.10  2008/02/15 10:17:57  xavela
// xavier :
// - command abort added for SCPI Keithleys
//
// Revision 1.9  2008/02/15 10:15:18  xavela
// xavier :
// - command abort added for SCPI Keithleys
//
// Revision 1.8  2008/02/15 08:49:51  xavela
// xavier :
// - minor change for integration time
//
// Revision 1.7  2008/02/13 15:51:43  xavela
// xavier :
// - Integration Mode available for DDC/SCPI devices
// - tests done with a K617 (DDC) and a K6485 (SCPI)
//
// Revision 1.6  2008/02/11 16:55:04  xavela
// xavier : for DDC part
// - integration mode OK (tests done with K_486)
//
// - TODO: DDC/SCPI
// integration time for ScanServer compatibility.
// report config for all DDC
// add configuration in SCPI start command
//
// Revision 1.5  2008/02/08 17:24:33  xavela
// xavier : for DDC part
// - trigger mode and buffer size management added.
// - TODO: DDC/SCPI
// integration time for ScanServer compatibility.
// SRQ management for Keithley Integration mode
//
// Revision 1.4  2007/11/28 10:37:20  xavela
// compile en MODE debug ->
// modifier le(s) MakeFile !
//
// Revision 1.3  2007/11/27 15:23:02  xavela
// xavier :
// - modif done on Novelec part :
// bug fix
//
// Revision 1.2  2007/11/27 10:17:46  xavela
// xavier :
// - modif done on Novelec part :
// init_MCCE2() added
//
// Revision 1.1  2007/07/09 13:20:37  stephle
// initial import
//
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <sstream>
#include <string>
#include <yat/utils/XString.h>
#include "AbstractElectrometerClass.h"
#include "ElectrometerException.h"

// ============================================================================
// AbstractElectrometerClass::AbstractElectrometerClass
// ============================================================================
AbstractElectrometerClass::AbstractElectrometerClass (std::string comLink_device_name)
:	electrometerState(UNKNOWN),
	_range(0),
	_rangeStr("NOT INITIALISED"),
	_mode("NOT INITIALISED"),
	_device_proxy_name(comLink_device_name),
	_electrometerProtocol(0),
	_size(-1),
	_trigMod(-1)

{
//	std::cout << "AbstractElectrometerClass::AbstractElectrometerClass <-" << std::endl;


//	std::cout << "AbstractElectrometerClass::AbstractElectrometerClass ->" << std::endl;
}

// ============================================================================
// AbstractElectrometerClass::~AbstractElectrometerClass
// ============================================================================
AbstractElectrometerClass::~AbstractElectrometerClass (void)
{
//	std::cout << "AbstractElectrometerClass::~AbstractElectrometerClass <-" << std::endl;

	if(_electrometerProtocol)
	{
		delete _electrometerProtocol;
		_electrometerProtocol = 0;
	}

//	std::cout << "AbstractElectrometerClass::~AbstractElectrometerClass ->" << std::endl;
}

// ============================================================================
// AbstractElectrometerClass::operator=
// ============================================================================
AbstractElectrometerClass& AbstractElectrometerClass::operator= (const AbstractElectrometerClass& src)
{
  //- no self assign
  if (this == &src) {
    return *this;
  }

  this->_device_proxy_name	  = src._device_proxy_name;
  this->_electrometerProtocol = src._electrometerProtocol;

  return *this;
}


// ============================================================================
// AbstractElectrometerClass::zero_check_on
// ============================================================================
void AbstractElectrometerClass::zero_check_on (void)
{
	//- send command
	_electrometerProtocol->zero_check_on( );
}
// ============================================================================
// AbstractElectrometerClass::zero_check_off
// ============================================================================
void AbstractElectrometerClass::zero_check_off (void)
{
	//- send command
	_electrometerProtocol->zero_check_off( );
}

// ============================================================================
// AbstractElectrometerClass::zero_correct_on
// ============================================================================
void AbstractElectrometerClass::zero_correct_on (void)
{
	//- send command
	_electrometerProtocol->zero_correct_on( );
}

// ============================================================================
// AbstractElectrometerClass::zero_correct_off
// ============================================================================
void AbstractElectrometerClass::zero_correct_off (void)
{
	//- send command
	_electrometerProtocol->zero_correct_off( );
}

// ============================================================================
// AbstractElectrometerClass::autoRange_ON
// ============================================================================
void AbstractElectrometerClass::autoRange_on (void)
{
	_electrometerProtocol->autoRange_on( );
}

// ============================================================================
// AbstractElectrometerClass::autoRange_OFF
// ============================================================================
void AbstractElectrometerClass::autoRange_off (void)
{
	_electrometerProtocol->autoRange_off( );
}

// ============================================================================
// AbstractElectrometerClass::is_autoRange_on
// ============================================================================
std::string AbstractElectrometerClass::is_autoRange_on (void)
{
	return _electrometerProtocol->is_autoRange_on( );
}

// ============================================================================
// AbstractElectrometerClass::auto_zero_on
// ============================================================================
void AbstractElectrometerClass::auto_zero_on (void)
{
	_electrometerProtocol->auto_zero_on( );
}

// ============================================================================
// AbstractElectrometerClass::auto_zero_off
// ============================================================================
void AbstractElectrometerClass::auto_zero_off (void)
{
	_electrometerProtocol->auto_zero_off( );
}

// ============================================================================
// AbstractElectrometerClass::init_keithley
// ============================================================================
void AbstractElectrometerClass::init_keithley (void)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::init_keithley( ).");
}

// ============================================================================
// AbstractElectrometerClass::abort
// ============================================================================
void AbstractElectrometerClass::abort (void)
{
	_electrometerProtocol->abort( );
}

// ============================================================================
// AbstractElectrometerClass::set_knplc
// ============================================================================
void AbstractElectrometerClass::set_knplc (float)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::set_knplc( ).");
}

// ============================================================================
// AbstractElectrometerClass::get_knplc
// ============================================================================
std::string AbstractElectrometerClass::get_knplc (void)
{
	return _electrometerProtocol->get_knplc( );
}

// ============================================================================
// AbstractElectrometerClass::set_triggerMode
// ============================================================================
void AbstractElectrometerClass::set_triggerMode (short)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::set_triggerMode( ).");
}

// ============================================================================
// AbstractElectrometerClass::set_triggercount
// ============================================================================
void AbstractElectrometerClass::set_triggercount (short )
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::set_triggercount( ).");
}

// ============================================================================
// AbstractElectrometerClass::get_triggercount
// ============================================================================
std::string AbstractElectrometerClass::get_triggercount (void)
{
	return _electrometerProtocol->get_triggercount( );
}

// ============================================================================
// AbstractElectrometerClass::set_triggerdelay
// ============================================================================
void AbstractElectrometerClass::set_triggerdelay (std::string trigDelStr)
{
	_electrometerProtocol->set_triggerdelay(trigDelStr);
}

// ============================================================================
// AbstractElectrometerClass::get_triggerdelay
// ============================================================================
std::string AbstractElectrometerClass::get_triggerdelay (void)
{
	return _electrometerProtocol->get_triggerdelay( );
}

// ============================================================================
// AbstractElectrometerClass::set_triggerdelayAuto
// ============================================================================
void AbstractElectrometerClass::set_triggerdelayAuto (std::string trigDelAutoStr)
{
	_electrometerProtocol->set_triggerdelayAuto(trigDelAutoStr);
}

// ============================================================================
// AbstractElectrometerClass::get_triggerdelayAuto
// ============================================================================
std::string AbstractElectrometerClass::get_triggerdelayAuto (void)
{
	return _electrometerProtocol->get_triggerdelayAuto( );
}

// ============================================================================
// AbstractElectrometerClass::set_averagecount
// ============================================================================
void AbstractElectrometerClass::set_averagecount (std::string averCountStr)
{
	_electrometerProtocol->set_averagecount(averCountStr);
}

// ============================================================================
// AbstractElectrometerClass::get_averagecount
// ============================================================================
std::string AbstractElectrometerClass::get_averagecount (void)
{
	return _electrometerProtocol->get_averagecount( );
}

// ============================================================================
// AbstractElectrometerClass::set_averagecontrol
// ============================================================================
void AbstractElectrometerClass::set_averagecontrol (std::string averCtrlStr)
{
	_electrometerProtocol->set_averagecontrol(averCtrlStr);
}

// ============================================================================
// AbstractElectrometerClass::get_averagecontrol
// ============================================================================
std::string AbstractElectrometerClass::get_averagecontrol (void)
{
	return _electrometerProtocol->get_averagecontrol( );
}

// ============================================================================
// AbstractElectrometerClass::clear_registers
// ============================================================================
void AbstractElectrometerClass::clear_registers (void)
{
	_electrometerProtocol->clear_registers();
}

// ============================================================================
// AbstractElectrometerClass::averageStateON
// ============================================================================
void AbstractElectrometerClass::averageStateON (void)
{
	_electrometerProtocol->averageStateON();
}

// ============================================================================
// AbstractElectrometerClass::averageStateOFF
// ============================================================================
void AbstractElectrometerClass::averageStateOFF (void)
{
	_electrometerProtocol->averageStateOFF();
}

// ============================================================================
// AbstractElectrometerClass::get_value
// ============================================================================
std::string AbstractElectrometerClass::get_value (void)
{
	std::string argout("");
	argout = _electrometerProtocol->get_value( );

	return argout;
}

// ============================================================================
// AbstractElectrometerClass::get_integratedValue
// ============================================================================
std::vector<double> AbstractElectrometerClass::get_integratedValue (void)
{
	return _electrometerProtocol->get_integratedValue( );
}

// ============================================================================
// AbstractElectrometerClass::get_fetchValue
// ============================================================================
std::vector<double> AbstractElectrometerClass::get_fetchValue (void)
{
	return _electrometerProtocol->get_fetchValue( );
}

// ============================================================================
// AbstractElectrometerClass::reset
// ============================================================================
void AbstractElectrometerClass::reset (void)
{
	_electrometerProtocol->reset( );
}

// ============================================================================
// AbstractElectrometerClass::local
// ============================================================================
void AbstractElectrometerClass::local (void)
{
	_electrometerProtocol->local( );
}

// ============================================================================
// AbstractElectrometerClass::remote
// ============================================================================
void AbstractElectrometerClass::remote (void)
{
	_electrometerProtocol->remote( );
}

// ============================================================================
// AbstractElectrometerClass::get_ElectroMeterMode
// ============================================================================
std::string AbstractElectrometerClass::get_ElectroMeterMode (void)
{
	return _electrometerProtocol->get_mode( );
}

// ============================================================================
// AbstractElectrometerClass::get_ElectroMeterRange
// ============================================================================
std::string AbstractElectrometerClass::get_ElectroMeterRange (void)
{
	return _electrometerProtocol->get_range( );
}

// ============================================================================
// AbstractElectrometerClass::get_overloadRangeState
// ============================================================================
bool AbstractElectrometerClass::get_overloadRangeState (void)
{
	return _electrometerProtocol->get_overloadRangeState( );
}

// ============================================================================
// AbstractElectrometerClass::get_configuration
// ============================================================================
std::string AbstractElectrometerClass::get_configuration (void)
{
	return _electrometerProtocol->get_DDC_configuration( );
}

// ============================================================================
// AbstractElectrometerClass::electrometer_status
// ============================================================================
std::string AbstractElectrometerClass::electrometer_status (void)
{
	/**
	*	This command is sent for SCPI devices only, so the SCPI State is updated here
	*/
	std::string argout("no data");

	try
	{
		argout = _electrometerProtocol->get_raw_status( );
		//- if no error the error code is 0
		if( yat::XString<short>::to_num(&argout[0]) != ON)
			set_electroState(ALARM);
		else
			set_electroState(ON);
	}
	catch(...)
	{
		set_electroState(FAULT);
		throw;
	}

	return argout;
}

// ============================================================================
// AbstractElectrometerClass::set_buffer_size
// ============================================================================
void AbstractElectrometerClass::set_buffer_size (short )
{
  // force read of range on instrument to update _range variable
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
                      "This Keithley device does not support this command.",
                      "AbstractElectrometerClass::set_buffer_size( ).");

}

// ============================================================================
// AbstractElectrometerClass::get_buffer_size
// ============================================================================
short AbstractElectrometerClass::get_buffer_size ()
{
  std::string size = _electrometerProtocol->get_buffer_size();

  return yat::XString<short>::to_num(size);
}

// ============================================================================
// AbstractElectrometerClass::set_integrationTime
// ============================================================================
void AbstractElectrometerClass::set_integrationTime (double)
{
  // force read of range on instrument to update _range variable
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
                      "This Keithley device does not support this command.",
                      "AbstractElectrometerClass::set_integrationTime( ).");

}

// ============================================================================
// AbstractElectrometerClass::clear_buffer
// ============================================================================
void AbstractElectrometerClass::clear_buffer (void)
{
  _electrometerProtocol->clear_buffer();
}

// ============================================================================
// AbstractElectrometerClass::store_raw_input
// ============================================================================
void AbstractElectrometerClass::store_raw_input (void)
{
  _electrometerProtocol->store_raw_input();
}

// ============================================================================
// AbstractElectrometerClass::start_storing
// ============================================================================
void AbstractElectrometerClass::start_storing (void)
{
  _electrometerProtocol->start_storing();
}

// ============================================================================
// AbstractElectrometerClass::enable_SRQBufferFull
// ============================================================================
void AbstractElectrometerClass::enable_SRQBufferFull (void)
{
  _electrometerProtocol->enable_SRQBufferFull();
}

// ============================================================================
// AbstractElectrometerClass::disable_SRQBufferFull
// ============================================================================
void AbstractElectrometerClass::disable_SRQBufferFull (void)
{
  _electrometerProtocol->disable_SRQBufferFull();
}

// ============================================================================
// AbstractElectrometerClass::SRQLineState
// ============================================================================
bool AbstractElectrometerClass::SRQLineState (void)
{
  return _electrometerProtocol->SRQLineState();
}

// ============================================================================
// AbstractElectrometerClass::readStatusByteRegister
// ============================================================================
short AbstractElectrometerClass::readStatusByteRegister (void)
{
  return _electrometerProtocol->readStatusByteRegister();
}

// ============================================================================
// AbstractElectrometerClass::MODE -> Ampere, Volt, Ohm, Coulomb and V on I meters
// The general case is that following functions are not supported
//	When supported by a particular electrometer, these methods must be defined in the
// corresponding electrometer class
// ============================================================================
// ============================================================================
// AbstractElectrometerClass::setAmperMeterMode
// ============================================================================
void AbstractElectrometerClass::setAmperMeterMode (void)
{
	_electrometerProtocol->setAmperMeterMode( );
/*	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::setAmperMeterMode( ).");
*/}

// ============================================================================
// AbstractElectrometerClass::setVoltMeterMode
// ============================================================================
void AbstractElectrometerClass::setVoltMeterMode (void)
{
	_electrometerProtocol->setVoltMeterMode( );
/*	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::setAmperMeterMode( ).");
*/}

// ============================================================================
// AbstractElectrometerClass::setOhmMeterMode
// ============================================================================
void AbstractElectrometerClass::setOhmMeterMode (void)
{
	_electrometerProtocol->setOhmMeterMode( );
/*	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::setAmperMeterMode( ).");
*/}


// ============================================================================
// AbstractElectrometerClass::setCoulombMeterMode
// ============================================================================
void AbstractElectrometerClass::setCoulombMeterMode (void)
{
	_electrometerProtocol->setCoulombMeterMode( );
/*	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::setAmperMeterMode( ).");
*/}

// ============================================================================
// AbstractElectrometerClass::enable_VSourceOutput
// ============================================================================
void AbstractElectrometerClass::enable_VSourceOutput (void)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::enable_VSourceOutput( ).");
}

// ============================================================================
// AbstractElectrometerClass::disable_VSourceOutput
// ============================================================================
void AbstractElectrometerClass::disable_VSourceOutput (void)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::disable_VSourceOutput( ).");
}

// ============================================================================
// AbstractElectrometerClass::set_VSourceValue
// ============================================================================
void AbstractElectrometerClass::set_VSourceValue (double)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::set_VSourceValue( ).");
}

// ============================================================================
// AbstractElectrometerClass::get_VSourceValue
// ============================================================================
double AbstractElectrometerClass::get_VSourceValue (void)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"This Electrometer does not support this command.",
											"AbstractElectrometerClass::get_VSourceValue( ).");
}

// ============================================================================
// AbstractElectrometerClass::get_ElectroMeterPolarity
// ============================================================================
std::string AbstractElectrometerClass::get_ElectroMeterPolarity (void)
{
	return _electrometerProtocol->get_polarity( );
}

// ============================================================================
// AbstractElectrometerClass::get_ElectroMeterFrequency
// ============================================================================
std::string AbstractElectrometerClass::get_ElectroMeterFrequency (void)
{
	return _electrometerProtocol->get_frequency( );
}

// ============================================================================
// AbstractElectrometerClass::get_ElectroMeterGain
// ============================================================================
std::string AbstractElectrometerClass::get_ElectroMeterGain (void)
{
	return _electrometerProtocol->get_gain( );
}

// ============================================================================
// AbstractElectrometerClass::set_ElectroMeterRange
// ============================================================================
void AbstractElectrometerClass::set_ElectroMeterRange (std::size_t rangeIdx)
{
    _electrometerProtocol->set_range(rangeIdx);
}

// ============================================================================
// AbstractElectrometerClass::set_ElectroMeterRange
// ============================================================================
void AbstractElectrometerClass::set_ElectroMeterRange (std::string rang)
{
    _electrometerProtocol->set_range(rang);
}

// ============================================================================
// AbstractElectrometerClass::set_ElectroMeterPolarity
// ============================================================================
void AbstractElectrometerClass::set_ElectroMeterPolarity (std::string pola)
{
	_electrometerProtocol->set_polarity(pola);
}

// ============================================================================
// AbstractElectrometerClass::set_ElectroMeterFrequency
// ============================================================================
void AbstractElectrometerClass::set_ElectroMeterFrequency (std::string freq)
{
	_electrometerProtocol->set_frequency(freq);
}

// ============================================================================
// AbstractElectrometerClass::set_ElectroMeterGain
// ============================================================================
void AbstractElectrometerClass::set_ElectroMeterGain (std::string gain)
{
	_electrometerProtocol->set_gain(gain);
}

// ============================================================================
// AbstractElectrometerClass::set_Zero_VonF_function
// ============================================================================
void AbstractElectrometerClass::set_Zero_VonF_function (void)
{
	_electrometerProtocol->unable_zeroVF_func( );
}

// ============================================================================
// AbstractElectrometerClass::set_Offset_ZeroV1_function
// ============================================================================
void AbstractElectrometerClass::set_Offset_ZeroV1_function (void)
{
	_electrometerProtocol->unable_offset_zeroV1_func( );
}

// ============================================================================
// AbstractElectrometerClass::set_Leakage_ZeroV2_function
// ============================================================================
void AbstractElectrometerClass::set_Leakage_ZeroV2_function (void)
{
	_electrometerProtocol->unable_leakage_zeroV2_func( );
}

// ============================================================================
// AbstractElectrometerClass::set_Test_function
// ============================================================================
void AbstractElectrometerClass::set_Test_function (void)
{
	_electrometerProtocol->unable_test_func( );
}

// ============================================================================
// AbstractElectrometerClass::set_Measure_function
// ============================================================================
void AbstractElectrometerClass::set_Measure_function (void)
{
	_electrometerProtocol->unable_measure_func( );
}

// ============================================================================
// AbstractElectrometerClass::mcce_on
// ============================================================================
void AbstractElectrometerClass::mcce_on (void)
{
	_electrometerProtocol->switch_MCCE2_ON( );
}

// ============================================================================
// AbstractElectrometerClass::mcce_off
// ============================================================================
void AbstractElectrometerClass::mcce_off (void)
{
	_electrometerProtocol->switch_MCCE2_OFF( );
}

// ============================================================================
// AbstractElectrometerClass::mcce_init
// ============================================================================
void AbstractElectrometerClass::mcce_init (void)
{
	_electrometerProtocol->init_MCCE2_for_communication( );
}

// ============================================================================
// AbstractElectrometerClass::set_ElectroChannel
// ============================================================================
void AbstractElectrometerClass::set_ElectroChannel (unsigned short address)
{
	_electrometerProtocol->set_electrometer_active_channel(address);
}

// ============================================================================
// AbstractElectrometerClass::get_ElectroChannel
// ============================================================================
unsigned short AbstractElectrometerClass::get_ElectroChannel (void)
{
	return _electrometerProtocol->get_electrometer_active_channel();
}

// ============================================================================
// AbstractElectrometerClass::save_configuration
// ============================================================================
void AbstractElectrometerClass::save_configuration (unsigned short memoryIdx)
{
	_electrometerProtocol->save_configuration(memoryIdx);
}

// ============================================================================
// AbstractElectrometerClass::restore_configuration
// ============================================================================
void AbstractElectrometerClass::restore_configuration (unsigned short memoryIdx)
{
	_electrometerProtocol->restore_configuration(memoryIdx);
}

