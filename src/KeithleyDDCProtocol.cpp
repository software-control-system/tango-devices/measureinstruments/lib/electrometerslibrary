// ============================================================================
//
// = CONTEXT
//    TANGO Project - DDC KeithleyDDCProtocol Support Library
//
// = FILENAME
//    KeithleyDDCProtocol.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <sstream>
#include <string>
#include <Xstring.h>
#include "KeithleyDDCProtocol.h"

// ============================================================================
// KeithleyDDCProtocol::KeithleyDDCProtocol
// ============================================================================
KeithleyDDCProtocol::KeithleyDDCProtocol (std::string& gpib_device_name)
:	ElectrometerProtocol(),
  _communication_link(0),
  _is_overloaded(false)
{
	std::cout << "KeithleyDDCProtocol::KeithleyDDCProtocol <-" << std::endl;

  _commDevName = gpib_device_name;

	std::cout << "KeithleyDDCProtocol::KeithleyDDCProtocol -> " << _commDevName << std::endl;
}

// ============================================================================
// KeithleyDDCProtocol::~KeithleyDDCProtocol
// ============================================================================
KeithleyDDCProtocol::~KeithleyDDCProtocol (void)
{
	std::cout << "KeithleyDDCProtocol::~KeithleyDDCProtocol <-" << std::endl;

  if(_communication_link)
	{
		delete _communication_link;
		_communication_link = 0;
	}

	std::cout << "KeithleyDDCProtocol::~KeithleyDDCProtocol ->" << std::endl;
}

// ============================================================================
// KeithleyDDCProtocol::build_communicationLink
// ============================================================================
bool KeithleyDDCProtocol::build_communicationLink()
{
  if (_commDevName.empty())
    return false;

	_communication_link = new TangoGpibLink (_commDevName);

  if (!_communication_link)
    return false;

  //-   prefix is enabled : this to the overload state !
  this->enable_readingWithPrefix();

  return true;
}

// ============================================================================
// KeithleyDDCProtocol::set_range
// ============================================================================
void KeithleyDDCProtocol::set_range (std::string value)
{
std::stringstream cmd_to_send;

	//- send command to Keithley device
	cmd_to_send << "R" << value << "X" << std::ends;
	_communication_link->write(cmd_to_send.str());

}

// ============================================================================
// KeithleyDDCProtocol::get_range
// ============================================================================
std::string KeithleyDDCProtocol::get_range (void)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"For DDC Keithley type, please check the status.",
											"KeithleyDDCProtocol::get_range( ).");
}

// ============================================================================
// KeithleyDDCProtocol::zero_check_on
// ============================================================================
void KeithleyDDCProtocol::zero_check_on (void)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "C1X" << std::ends;
	_communication_link->write(cmd_to_send.str());

}
// ============================================================================
// KeithleyDDCProtocol::zero_check_off
// ============================================================================
void KeithleyDDCProtocol::zero_check_off (void)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "C0X" << std::ends;
	_communication_link->write(cmd_to_send.str());
}


// ============================================================================
// KeithleyDDCProtocol::zero_correct_on
// ============================================================================
void KeithleyDDCProtocol::zero_correct_on (void)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "Z1X" << std::ends;
	_communication_link->write(cmd_to_send.str());

}

// ============================================================================
// KeithleyDDCProtocol::zero_correct_off
// ============================================================================
void KeithleyDDCProtocol::zero_correct_off (void)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "Z0X" << std::ends;
	_communication_link->write(cmd_to_send.str());

}

// ============================================================================
// KeithleyDDCProtocol::auto_zero_on
// ============================================================================
void KeithleyDDCProtocol::auto_zero_on (void)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"DDC Keithley does not support this command.",
											"KeithleyDDCProtocol::auto_zero_on( ).");

}

// ============================================================================
// KeithleyDDCProtocol::auto_zero_off
// ============================================================================
void KeithleyDDCProtocol::auto_zero_off (void)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"DDC Keithley does not support this command.",
											"KeithleyDDCProtocol::auto_zero_off( ).");

}

// ============================================================================
// KeithleyDDCProtocol::autoRange_ON
// ============================================================================
void KeithleyDDCProtocol::autoRange_on (void)
{
std::stringstream cmd_to_send;

	cmd_to_send << "R0X" << std::ends;
	_communication_link->write(cmd_to_send.str());
}

// ============================================================================
// KeithleyDDCProtocol::autoRange_OFF
// ============================================================================
void KeithleyDDCProtocol::autoRange_off (void)
{
	throw electrometer::ElectrometerException("COMMAND_NOT_SUPPORTED",
											"DDC Keithley does not support this command.",
											"KeithleyDDCProtocol::autoRange_off( ).");
}

// ============================================================================
// KeithleyDDCProtocol::autoRange_OFF for Keithley 486 & 487
// ============================================================================
void KeithleyDDCProtocol::autoRange_OFF_forK486_487 (void)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "R10X" << std::ends;
	_communication_link->write(cmd_to_send.str());
}

// ============================================================================
// KeithleyDDCProtocol::autoRange_OFF for Keithley 617 & 6512
// ============================================================================
void KeithleyDDCProtocol::autoRange_OFF_forK617_6512 (void)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "R12X" << std::ends;
	_communication_link->write(cmd_to_send.str());
}

// ============================================================================
// KeithleyDDCProtocol::MODE -> Ampere, Volt, Ohm, Coulomb and V on I meters
// ============================================================================
// ============================================================================
// KeithleyDDCProtocol::setAmperMeterMode (617 & 6512 Keithley DDC models
// ============================================================================
void KeithleyDDCProtocol::setAmperMeterMode (void)
{
std::stringstream cmd_to_send;

	//- mode current
	_mode = "CURRent";

	//- send command
	cmd_to_send << "F1X" << std::ends;
	_communication_link->write(cmd_to_send.str());
}

// ============================================================================
// KeithleyDDCProtocol::setAmperMeterMode (487 Keithley DDC model
// ============================================================================
void KeithleyDDCProtocol::setAmperMeterMode_forK487 (void)
{
std::stringstream cmd_to_send;

	//- mode current
	_mode = "CURRent";

	//- send command
	cmd_to_send << "F0X" << std::ends;
	_communication_link->write(cmd_to_send.str());
}

// ============================================================================
// KeithleyDDCProtocol::setVoltMeterMode (617 & 6512 Keithley DDC models
// ============================================================================
void KeithleyDDCProtocol::setVoltMeterMode (void)
{
std::stringstream cmd_to_send;

	//- mode volt
	_mode = "VOLTage";

	//- send command
	cmd_to_send << "F0X" << std::ends;
	_communication_link->write(cmd_to_send.str());

}

// ============================================================================
// KeithleyDDCProtocol::setOhmMeterMode (617 & 6512 Keithley DDC models
// ============================================================================
void KeithleyDDCProtocol::setOhmMeterMode (void)
{
std::stringstream cmd_to_send;

	//- send command
	//- mode ohm
	_mode = "RESistance";

	cmd_to_send << "F2X" << std::ends;
	_communication_link->write(cmd_to_send.str());

}

// ============================================================================
// KeithleyDDCProtocol::setCoulombMeterMode (617 & 6512 Keithley DDC models
// ============================================================================
void KeithleyDDCProtocol::setCoulombMeterMode (void)
{
std::stringstream cmd_to_send;

	//- mode coulomb
	_mode = "CHARge";

	//- send command
	cmd_to_send << "F3X" << std::ends;
	_communication_link->write(cmd_to_send.str());
}

// ============================================================================
// KeithleyDDCProtocol::get_value
// ============================================================================
std::string KeithleyDDCProtocol::get_value (void)
{
std::stringstream cmd_to_send;
std::string argout("no data");

  //- get data value
  cmd_to_send << "X" << std::ends;
  argout = _communication_link->write_read(cmd_to_send.str());
  //XE argout = _communication_link->read();

  //- check if device range is overloaded : First char of the prefix !
  if(argout[0] == 'O') //- O means overload !
    _is_overloaded = true;
  else
    _is_overloaded = false;

  //- return value : data begin at idx 4
  argout = argout.substr(4);
	return argout;
}

// ============================================================================
// KeithleyDDCProtocol::get_integratedValue
// ============================================================================
std::vector<double> KeithleyDDCProtocol::get_integratedValue (void)
{
std::string tmp("");

	//- This command queries the bufferised values after an integration
	tmp = _communication_link->read();

  //- extract all data
  return buildDataList(tmp);
}

// ============================================================================
// KeithleyDDCProtocol::get_fetchValue
// ============================================================================
std::vector<double> KeithleyDDCProtocol::get_fetchValue (void)
{
std::string tmp("");

	//- This command queries the last bufferised value(s)
	tmp = _communication_link->read();

  //- extract all data
  return buildDataList(tmp);

}

// ============================================================================
// KeithleyDDCProtocol::buildDataList
//
//  This method extract and convert data from the received string
// ============================================================================
std::vector<double> KeithleyDDCProtocol::buildDataList(std::string datalist)
{
std::string dataStr("");
std::vector<double> argout;
std::string::size_type posBeg;
std::string::size_type posEnd;

	//-
	posBeg = 0;
	posEnd = datalist.find(',', posBeg);

  //- there is just one value
  if(posEnd == std::string::npos)
  {
		  argout.push_back( XString<double>::convertFromString(datalist) );
  }
  else
  {
    for(; posEnd != std::string::npos; )
    {
		  dataStr = datalist.substr(posBeg,posEnd);
		  argout.push_back( XString<double>::convertFromString(dataStr) );

      posBeg = posEnd+1;
		  posEnd = datalist.find(',', posBeg);

      //- end of string reached so latest data is here
      if(posEnd == std::string::npos)
      {
        dataStr = datalist.substr(posBeg);
   		  argout.push_back( XString<double>::convertFromString(dataStr) );
        break;
      }
    }
  }
	return argout;
}

// ============================================================================
// KeithleyDDCProtocol::reset
// ============================================================================
void KeithleyDDCProtocol::reset (void)
{
std::stringstream cmd_to_send;

	//- send command to return to default conditions !!
	cmd_to_send << "*RST" << std::ends;
	_communication_link->write(cmd_to_send.str());
}

// ============================================================================
// KeithleyDDCProtocol::get_raw_status
// ============================================================================
std::string KeithleyDDCProtocol::get_raw_status (void)
{
std::stringstream cmd_to_send;
std::string argout("no data");

	cmd_to_send.str("");
	//- get status word
	cmd_to_send << "U1X" << std::endl;
	argout = _communication_link->write_read(cmd_to_send.str());
  //- DDC devices are to slow to give a response for a write_read cmd
	//XE argout = _communication_link->read();

	return argout;
}

// ============================================================================
// KeithleyDDCProtocol::get_DDC_configuration
// ============================================================================
std::string KeithleyDDCProtocol::get_DDC_configuration (void)
{
std::stringstream cmd_to_send;
std::string argout("no data");

	cmd_to_send.str("");
	//- get status word
	cmd_to_send << "U0X" << std::endl;
	argout = _communication_link->write_read(cmd_to_send.str());
	//XE argout = _communication_link->read();

	return argout;
}

// ============================================================================
// KeithleyDDCProtocol::clear_registers
// ============================================================================
void KeithleyDDCProtocol::clear_registers (void)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "*CLS" << std::ends;
	_communication_link->write(cmd_to_send.str());

}

// ============================================================================
// KeithleyDDCProtocol::set_buffer_size()
// ============================================================================
void KeithleyDDCProtocol::set_buffer_size (std::string cmd)
{
	//- send command
	_communication_link->write(cmd);
}

// ============================================================================
// KeithleyDDCProtocol::set_triggerMode()
// ============================================================================
void KeithleyDDCProtocol::set_triggerMode (std::string cmd)
{
	//- send command
	_communication_link->write(cmd);
}

// ============================================================================
// KeithleyDDCProtocol::enable_SRQBufferFull()
// ============================================================================
void KeithleyDDCProtocol::enable_SRQBufferFull (void)
{
  std::string cmd_to_send("M2X");

	//- send command : enable buffer full in the Measurement event enable register
	_communication_link->write(cmd_to_send);

}

// ============================================================================
// KeithleyDDCProtocol::disable_SRQBufferFull()
// ============================================================================
void KeithleyDDCProtocol::disable_SRQBufferFull (void)
{
std::string cmd_to_send("M0X");

	//- send command : disable buffer full event
	_communication_link->write(cmd_to_send);

}

// ============================================================================
// KeithleyDDCProtocol::SRQLineState()
// ============================================================================
bool KeithleyDDCProtocol::SRQLineState (void)
{
	//- if asserted -> the programmed event occurs : the device can be asked !
	return _communication_link->SRQLineState();
}

// ============================================================================
// KeithleyDDCProtocol::readStatusByteRegister()
// ============================================================================
short KeithleyDDCProtocol::readStatusByteRegister (void)
{
	//- if asserted -> the programmed event occurs : the device can be asked !
	return _communication_link->readStatusByteRegister();
}

// ============================================================================
// KeithleyDDCProtocol::set_conversionRate()
// ============================================================================
void KeithleyDDCProtocol::set_conversionRate (void)
{
std::string cmd_to_send("Q0X");

	//- send command : ADC default conversion rate
	_communication_link->write(cmd_to_send);
}

// ============================================================================
// KeithleyDDCProtocol::enable_readingWithPrefix()
// ============================================================================
void KeithleyDDCProtocol::enable_readingWithPrefix (void)
{
std::string cmd_to_send("G0X");

	//- send command : ADC default conversion rate
	_communication_link->write(cmd_to_send);
}

// ============================================================================
// KeithleyDDCProtocol::disable_readingWithPrefix()
// ============================================================================
void KeithleyDDCProtocol::disable_readingWithPrefix (void)
{
std::string cmd_to_send("G1X");

	//- send command : ADC default conversion rate
	_communication_link->write(cmd_to_send);
}

// ============================================================================
// KeithleyDDCProtocol::enable_ReadingsFromElectrometer() -> disable storing mode
// ============================================================================
void KeithleyDDCProtocol::enable_ReadingsFromElectrometer (void)
{
std::string cmd_to_send("B0X");

	//- send command : readings from Electrometer
	_communication_link->write(cmd_to_send);

}

// ============================================================================
// KeithleyDDCProtocol::enable_readingsFromBuffer_K617_6512()
// ============================================================================
void KeithleyDDCProtocol::enable_readingsFromBuffer_K617_6512 (void)
{
std::string cmd_to_send("B1X");

	//- send command : ADC default conversion rate
	_communication_link->write(cmd_to_send);
}

// ============================================================================
// KeithleyDDCProtocol::enable_readingsFromBuffer_K486_487()
// ============================================================================
void KeithleyDDCProtocol::enable_readingsFromBuffer_K486_487 (void)
{
std::string cmd_to_send("B2X");

	//- send command : ADC default conversion rate
	_communication_link->write(cmd_to_send);
}

// ============================================================================
// KeithleyDDCProtocol::enable_integrationPeriod()
// ============================================================================
void KeithleyDDCProtocol::enable_integrationPeriod (void)
{
std::string cmd_to_send("S1X");

  //-
	//- send command : this command is only for k_486 or K_487
  //- S1 is for : "Line cycle integration period (5-I /2d resolution)"
	_communication_link->write(cmd_to_send);
}


/*// ============================================================================
// KeithleyDDCProtocol::init_keithley to perform reading(s) data
// ============================================================================
void KeithleyDDCProtocol::init_keithley (void)
{
  //- Set default conversion rate (internal keithley device conversion rate)
  this->set_conversionRate();
  //- Set default conversion rate (internal keithley device conversion rate)
  this->set_conversionRate();
  //- Disable readings with prefix
  this->disable_readingWithPrefix();

	//- send command : INIT to trigg readings !
	_communication_link->write(cmd_to_send);
}

// Following functions are part of commands supported via the DDC protocol.
// We may have to implement them after first tests on beamlines

// ============================================================================
// KeithleyDDCProtocol::setExternalFeedbackMeterMode (617 & 6512 Keithley DDC models
// ============================================================================
void KeithleyDDCProtocol::setExternalFeedbackMeterMode (void)
		throw (electrometer::ElectrometerException, Tango::DevFailed)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "F4X" << std::ends;
	_communication_link->write(cmd_to_send.str());

}

// ============================================================================
// KeithleyDDCProtocol::setVonIMeterMode (only 617 Keithley DDC model
// ============================================================================
void KeithleyDDCProtocol::setVonIMeterMode (void)
		throw (electrometer::ElectrometerException, Tango::DevFailed)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "F5X" << std::ends;
	_communication_link->write(cmd_to_send.str());

}

// ============================================================================
// KeithleyDDCProtocol::setVonIMeterMode (only 487 Keithley DDC model
// ============================================================================
void KeithleyDDCProtocol::setVonIMeterMode_forK487 (void)
		throw (electrometer::ElectrometerException, Tango::DevFailed)
{
std::stringstream cmd_to_send;

	//- send command
	cmd_to_send << "F1X" << std::ends;
	_communication_link->write(cmd_to_send.str());

}

 ...  TO BE IMPLEMENTED !!!!!

// ============================================================================
// KeithleyDDCProtocol::reading_source
// ============================================================================
void KeithleyDDCProtocol::reading_source (void)
		throw (electrometer::ElectrometerException, Tango::DevFailed)
{

}

// ============================================================================
// KeithleyDDCProtocol::reading_source
// ============================================================================
void KeithleyDDCProtocol::data_store (void)
		throw (electrometer::ElectrometerException, Tango::DevFailed)
{

}

// ============================================================================
// KeithleyDDCProtocol::reading_source
// ============================================================================
void KeithleyDDCProtocol::data_format (void)
		throw (electrometer::ElectrometerException, Tango::DevFailed)
{

}

// ============================================================================
// KeithleyDDCProtocol::reading_source
// ============================================================================
void KeithleyDDCProtocol::baseline_suppression_ON (void)
		throw (electrometer::ElectrometerException, Tango::DevFailed)
{

}

// ============================================================================
// KeithleyDDCProtocol::reading_source
// ============================================================================
void KeithleyDDCProtocol::baseline_suppression_OFF (void)
		throw (electrometer::ElectrometerException, Tango::DevFailed)
{

}
// ============================================================================
// KeithleyDDCProtocol::reading_source
// ============================================================================
void KeithleyDDCProtocol::keithley_status (void)
		throw (electrometer::ElectrometerException, Tango::DevFailed)
{

}
*/
